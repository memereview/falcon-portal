<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
	

Route::get('/admin', 'AdminController@index')->middleware('admin');
Route::get('/client', 'ClientController@index')->middleware('client');

//Login
Route::get('/login', 'SessionController@index');
Route::post('/login', 'SessionController@login');
Route::get('/logout', 'SessionController@logout');

//Forgot Password
Route::post('/forgot-password', 'PasswordController@sendResetToken');
Route::get('/reset-password/{token}', 'PasswordController@showResetForm');
Route::post('/reset-password/{token}', 'PasswordController@resetPassword');

Route::get('/admin', 'AdminController@index')->middleware('admin');

//User list/creation
Route::resource('/admin/users', 'UserController')->middleware('admin');
Route::patch('/admin/user/{id}', 'UserController@updatePassword')->middleware('admin');

// user verification
Route::get('/verify/{token}', 'UserController@showVerification');
Route::post('/verify/{token}', 'UserController@verify');

//Client Page
Route::get('/client', 'ClientController@index')->middleware('client');

//view client profile
Route::get('/client/details/{id}', 'ClientController@showDetails')->middleware('client');
Route::get('/client/change-password/{id}', 'ClientController@changePassword')->middleware('client');
Route::patch('/client/details/{id}', 'ClientController@updatePassword')->middleware('client');

//Upload Manifest
Route::get('/client/upload-manifest/', 'ClientController@showUploader')->middleware('client');
Route::post('/client/upload-manifest', 'ClientController@fileUploader')->middleware('client');
Route::get('/download/{file}', 'ClientController@downloadErrorFile');

//Manifest/Units Page
Route::get('/manifests', function() {
    return redirect('/manifests/new');
})->middleware('all');

Route::get('/manifests/new', 'ManifestController@showNew')->middleware('all');
Route::get('/manifests/current', 'ManifestController@showCurrent')->middleware('all');
Route::get('/manifests/archived', 'ManifestController@showArchives')->middleware('all');

Route::get('/manifests/new/units/{number}', 'ManifestController@showNewUnits')->middleware('all');
Route::get('/manifests/current/units/{number}', 'ManifestController@showCurrentUnits')->middleware('all');
Route::get('/manifests/archived/units/{number}', 'ManifestController@showArchivedUnits')->middleware('all');
// Route::get('/manifests/units/{number}', 'ManifestController@showUnits')->middleware('all');


//Verifying Units
Route::post('/manifests/{manifestNumber}/units/{trackingNumber}/verify', 'ManifestController@verifyUnit');

//Verifying Manifest
Route::post('/manifests/{manifestNumber}/verify', 'ManifestController@verifyManifest');

//Assign Units
Route::get('/admin/assign-units', 'AssignUnitsController@showUnits')->middleware('admin');
Route::post('/admin/assign-units', 'AssignUnitsController@assignUnits')->middleware('admin');

//Auto Assign
Route::post('/admin/auto-assign', 'AssignUnitsController@autoAssign')->middleware('admin');

//Clear Assign
Route::get('/admin/clear-assign', 'AssignUnitsController@clearAssign')->middleware('admin');

//Image
Route::get('/images/{image}', 'ImageController@index');
Route::post('/upload/profile-picture', 'ImageController@uploadProfilePicture')->middleware('all');

//Access Denied
Route::get('/403', 'ForbiddenController@index');

//Customer delivery confirmation
Route::get('/delivered/{token}', 'ConfirmationController@delivered');
Route::get('/failed/{token}', 'ConfirmationController@failed');
Route::get('/expired', 'ConfirmationController@expired');

//Delivery confirmation
Route::post('/units/{trackingNumber}/delivery-verify', 'ConfirmationController@verify');

//Header routes
//change password
Route::get('/change-password', 'ChangePasswordController@index');
Route::post('/change-password', 'ChangePasswordController@store');
// profile
Route::get('/profile', 'ProfileController@index');
Route::put('/profile', 'ProfileController@update');
//Developers Details
Route::get('/developers', 'DeveloperController@index');