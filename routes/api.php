<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Mobile app login
Route::post('/login', 'Api\SessionController@login');

Route::namespace('Api')->middleware('rider')->group(function () {

    // changing of password
    Route::put('/change-password', 'ChangePasswordController@update');

    // Retrieving the rider's assigned units
    Route::get('/units', 'UnitsController@assigned');

    // Delivering of the unit
    Route::post('/units/delivering', 'UnitsController@delivering');

    // Delivered the unit
    Route::post('/units/delivered', 'UnitsController@delivered');

    // Returned the unit
    Route::post('/units/returned', 'UnitsController@returned');

});
