@extends('layouts.master', ['title' => 'Change Password'])

@section('content')

	<form action="/client/details/{{$client->id}}" method="post" novalidate>
		{{ method_field('PATCH') }}
		{{ csrf_field() }}

	<h1>Change Password</h1>
	
	<div class="panel-body">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
    </div>

    <div>
        <label for="current_password">Current Password:
            <input id="current_password" type="password" name="current_password">
            @if ($errors->has('current_password'))
                <span>
                    {{ $errors->get('current_password')[0] }}
                </span>
            @endif
        </label>
    </div>
    <div>
        <label for="new_password">New Password:
            <input id="new_password" type="password" name="new_password">
            @if ($errors->has('new_password'))
                <span>
                    {{ $errors->get('new_password')[0] }}
                </span>
            @endif
        </label>
    </div>
    <div>
        <label for="new_password_confirmation">Confirm New Password:
            <input id="new_password_confirmation" type="password" name="new_password_confirmation">
            @if ($errors->has('password_confirmation'))
                <span>
                    {{ $errors->get('password_confirmation')[0] }}
                </span>
            @endif
        </label>
    </div>

    <button>Submit</button>
</form>

@endsection