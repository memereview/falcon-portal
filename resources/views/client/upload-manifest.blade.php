@extends('layouts.master', [
    'title' => 'Upload Manifest',
    'breadcrumbs' => [
        ['Upload Manifest', '/client/upload-manifest']
    ]
])

@section('content')

    <br>
    <br>
    <form method="POST" action="/client/upload-manifest" enctype="multipart/form-data" novalidate>
        {{ csrf_field() }}

        @if ($errors->has('manifest'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->get('manifest')[0] }}
            </div>
        @elseif ($errors->has('file_name'))
            <div class="alert alert-danger" role="alert">
                {{ $errors->get('file_name')[0] }}
            </div>
        @elseif ($errors->all())
            <div class="alert alert-danger">
                One or more Manifest data is incorrect.
                <a href="/download/{{ $filename }}">
                    Download Error Report
                </a>
            </div>
        @endif

        <input name="manifest" id="manifest" type="file" accept=".csv">
        <button class="btn btn-primary">Upload</button>
        <br>

    </form>
    <br>
    <p><b>Follow these steps to upload your manifest:</b></p>
    <p><b>1. Open your text editor you want</b></p>
    <p><b>2. Copy and paste this to your text editor, "Tracking Number,First Name,Middle Name,Last Name,Phone Number,Email,Street 1,Street 2,Barangay,City,Province,Postal Code,Collection,Remarks". This will be your guide in creating your manifest.</b></p>
    <p><b>3. Following the first line, enter the details of your units to be delivered, one line per unit (Only the Middle Name, Street 2, Barangay and Remarks are optional fields). Please follow the format above to avoid upload error reports</b></p>
    <p><b>4. Save your file as .csv or .txt</b></p>
    <p><b>5. Press the “Browse” button to choose a manifest to upload.</b></p>
    <p><b>6. After choosing a file, press the “Upload” button.</b></p>
    <br>
@endsection
