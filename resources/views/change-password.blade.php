@extends('layouts.master', [
    'title' => 'Change Password',
    'breadcrumbs' => [
        ['Change Password', '/change-password']
    ]
])

@section('content')

    @if (isset($success))
        <div class="row">
            <div class="m-4 p-3 col alert alert-success">
                {{ $success }}
            </div>
        </div>
    @endif

	<div class="p-4 row">
        <div class="col-4">
            <form action="/change-password" method="post" novalidate>
                @csrf

                <div class="form-group">
                    <label for="current-password" class="required">Current Password:</label>
                    <input type="password" id="current-password"
                           class="form-control {{ $errors->has('current_password') ? 'is-invalid' : '' }}"
                           name="current_password">
                    @if ($errors->has('current_password'))
                        <div class="invalid-feedback">
                            {{ $errors->get('current_password')[0] }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="new-password" class="required">New Password:</label>
                    <input type="password" id="new-password"
                           class="form-control {{ $errors->has('new_password') ? 'is-invalid' : '' }}"
                           name="new_password">
                    @if ($errors->has('new_password'))
                        <div class="invalid-feedback">
                            {{ $errors->get('new_password')[0] }}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="new-password-confirmation" class="required">Confirm New Password:</label>
                    <input type="password" id="new-password-confirmation"
                           class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}"
                           name="new_password_confirmation">
                    @if ($errors->has('password_confirmation'))
                        <div class="invalid-feedback">
                            {{ $errors->get('password_confirmation')[0] }}
                        </div>
                    @endif
                </div>

                <button class="mt-4 btn btn-primary">Change Password</button>
            </form>
        </div>
    </div>

@endsection