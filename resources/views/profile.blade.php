@extends('layouts.master', [
    'title' => 'Profile',
    'breadcrumbs' => [
        ['Profile', '/profile']
    ]
])

@section('content')

    @if (isset($success))
        <div class="row">
            <div class="m-4 p-3 col alert alert-success">
                {{ $success }}
            </div>
        </div>
    @endif

    <form id="profile" action="/profile" method="post" novalidate>
        @method('PUT')
        @csrf

        <div class="p-4 row">
            <div class="col text-center">
                @if (session()->get('user')['picture'])
                    <div id="profile-picture" class="rounded-circle picture" style="background-image: url('/images/{{ $user['picture'] }}')">
                        <input id="picture-input" type="file" name="picture" accept=".jpeg, .jpg, .png, .gif" hidden>
                    </div>
                @else
                    <div id="profile-picture" class="rounded-circle picture">
                        <input id="picture-input" type="file" name="picture" accept=".jpeg, .jpg, .png, .gif" hidden>
                    </div>
                @endif
            </div>
        </div>

        <div class="p-4 row">
            <table class="col">
                <tbody>
                <tr class="row mt-3">
                    <td class="col-5 pt-3 text-right">First Name:</td>
                    <td class="col-7 pt-1">
                        <div class="col-5">
                            <input type="text"
                                   class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}"
                                   name="first_name" value="{{ $user['first_name'] }}">
                            @if ($errors->has('first_name'))
                                <div class="invalid-feedback">
                                    {{ $errors->get('first_name')[0] }}
                                </div>
                            @endif
                        </div>
                    </td>
                </tr>
                <tr class="row mt-3">
                    <td class="col-5 pt-3 text-right">Middle Name:</td>
                    <td class="col-7 pt-1">
                        <div class="col-5">
                            <input type="text"
                                   class="form-control {{ $errors->has('middle_name') ? 'is-invalid' : '' }}"
                                   name="middle_name" value="{{ $user['middle_name'] }}">
                            @if ($errors->has('middle_name'))
                                <div class="invalid-feedback">
                                    {{ $errors->get('middle_name')[0] }}
                                </div>
                            @endif
                        </div>
                    </td>
                </tr>
                <tr class="row mt-3">
                    <td class="col-5 pt-3 text-right">Last Name:</td>
                    <td class="col-7 pt-1">
                        <div class="col-5">
                            <input type="text"
                                   class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}"
                                   name="last_name" value="{{ $user['last_name'] }}">
                            @if ($errors->has('last_name'))
                                <div class="invalid-feedback">
                                    {{ $errors->get('last_name')[0] }}
                                </div>
                            @endif
                        </div>
                    </td>
                </tr>
                <tr class="row mt-3">
                    <td class="col-5 pt-3 text-right">Email:</td>
                    <td class="col-7 pt-1">
                        <div class="col-5">
                            <input type="text"
                                   class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                   name="email" value="{{ $user['email'] }}">
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->get('email')[0] }}
                                </div>
                            @endif
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="p-4 text-left">
            <button class="mt-4 btn btn-primary">Update Profile</button>
        </div>
    </form>

@endsection