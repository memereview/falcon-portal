<h2>Greetings {{ $unit->customer->name }}!</h2>
<br/>
Please confirm if your item has been delivered to you on this address:
<br/>

{{ $unit->address->street }} {{ $unit->address->street2 }} {{ $unit->address->barangay }}
{{ $unit->address->city }}, {{ $unit->address->province }} ( {{ $unit->address->postal }} )

<table style="table-layout:fixed;margin-top:10px" cellspacing="0" cellpadding="0">
    <tbody>
        <tr>
            <td style="width:14.28%;border-right:2px #fff solid;padding:0;height:8px;background-color:#4e8d00"></td>
            <td style="width:14.28%;border-right:2px #fff solid;padding:0;height:8px;background-color:#e7340f"></td>
        </tr>

        <tr>
            <td style="width:14.28%;border-right:2px #fff solid;text-align:center;padding:16px 0px;background-color:#e6efdd">
                <a href="{{ $urlDelivered}} "> YES </a>
            </td>

            <td style="width:14.28%;border-right:2px #fff solid;text-align:center;padding:16px 0px;background-color:#fce3e0">
                <a href="{{ $urlFailed }} "> NO </a>
            </td>
        </tr>
    </tbody>
</table>

<div>Thank you for trusting Falcon!</div>

</a>