<h2>Greetings {{ $user->first_name }} {{ $user->last_name }}!</h2>
<br/>

This is to notify you that the manifest <b>{{ $filename }}</b> has successfully been uploaded
and ready for verification.

