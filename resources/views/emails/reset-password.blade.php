<h2>Greetings {{ $user->first_name . ' ' . $user->last_name }}!</h2>
<br/>
Please click on the link below to reset your password.
<br/>

<a href="{{ $url }}">Reset Password</a>

<div>If the link above did not work, copy the url below and paste it in your browser</div>

<a href="{{ $url }}">
    {{ $url }}
</a>