<h2>Welcome to Falcon, {{ $user->first_name . ' ' . $user->last_name }}!</h2>
<br/>
    Please click on the link below to verify your email account.
<br/>

<a href="{{ $url }}">Verify Account</a>

<div>If the link above did not work, copy the url below and paste it in your browser</div>

<a href="{{ $url }}">
    {{ $url }}
</a>