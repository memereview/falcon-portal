<h2>Greetings {{ $unit->customer->name }}!</h2>
<br/>
This is to notify you that your item is on its way to this address:
<br/>

{{ $unit->address->street }} {{ $unit->address->street2 }} {{ $unit->address->barangay }}
{{ $unit->address->city }}, {{ $unit->address->province }} ( {{ $unit->address->postal }} )

<div>Please wait for it to be delivered</div>

</a>