<nav>
    <a href="/">
        <div id="nav-title">
            <img src="/img/logo.png">

            <span id="app-name">
                {{ config('app.name', 'Falcon') }}
            </span>
        </div>
    </a>

    <ul id="nav-content" class="nav">
        @if (session()->get('user')['type'] === 'ADMIN')
            <li class="nav-menu {{ strpos(request()->path(), 'admin/users') === 0 ? 'selected' : '' }}">
                <a href="/admin/users">
                    <span class="menu-icon">
                        <i class="fas fa-user-circle"></i>
                    </span>
                    <span class="menu-text">
                        Manage Accounts
                    </span>
                </a>
            </li>
        @endif

        @if (session()->get('user')['type'] === 'CLIENT')
            <li class="nav-menu {{ request()->path() === 'client/upload-manifest' ? 'selected' : '' }}">
                <a href="/client/upload-manifest">
                    <span class="menu-icon">
                        <i class="fas fa-upload"></i>
                    </span>
                    <span class="menu-text">
                        Upload Manifest
                    </span>
                </a>
            </li>
        @endif

        <li class="nav-menu {{ strpos(request()->path(), 'manifest') === 0 ? 'selected' : '' }}">
            <a href="#"
               class="toggle-submenu {{ strpos(request()->path(), 'manifest') !== 0 ? 'collapsed' : '' }}"
               data-toggle="collapse" data-target="#manifests-submenu" aria-expanded="false">
                <span class="menu-icon">
                    <i class="fas fa-clipboard-list"></i>
                </span>
                <span class="menu-text">
                    Manifests
                </span>
                <span class="submenu-icon">
                    <i class="fas fa-angle-down fa-1x"></i>
                </span>
            </a>
            <div id="manifests-submenu" class="nav-submenu nav collapse {{ strpos(request()->path(), 'manifest') === 0 ? 'show' : '' }}">
                <ul>
                    <li class="{{ request()->path() === 'manifests/new' ? 'active' : '' }}">
                        <a href="/manifests/new">New</a>
                    </li>
                    <li class="{{ request()->path() === 'manifests/current' ? 'active' : '' }}">
                        <a href="/manifests/current">Current</a>
                    </li>
                    <li class="{{ request()->path() === 'manifests/archived' ? 'active' : '' }}">
                        <a href="/manifests/archived">Archived</a>
                    </li>
                </ul>
            </div>
        </li>

        @if (session()->get('user')['type'] === 'ADMIN')
            <li class="nav-menu {{ request()->path() === 'admin/assign-units' ? 'selected' : '' }}">
                <a href="/admin/assign-units">
                    <span class="menu-icon">
                        <i class="fas fa-clone"></i>
                    </span>
                    <span class="menu-text">
                        Assign Units
                    </span>
                </a>
            </li>
        @endif
    </ul>
</nav>