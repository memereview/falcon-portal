<header>
    <div class="navbar row">
        <ul class="col-1 navbar-nav header-left">
            <li>
                <a id="sidebar-toggle" href="#">
                    <i class="fas fa-bars"></i>
                </a>
            </li>
        </ul>
        <ul class="col-6 navbar-nav header-middle">
            <div class="row">
                <div id="date-container" class="col-8">
                    <i class="far fa-calendar-alt"></i>
                    <span id="date"></span>
                </div>
                <div class="col">
                    <i class="far fa-clock"></i>
                    <span id="time"></span>
                </div>
            </div>
        </ul>
        <!-- <ul class="col navbar-nav text-right">
            <li class="nav-item">
                <a href="#" id="notification">
                    <i class="far fa-envelope"></i>
                    <span class="badge badge-light">4</span>
                </a>
            </li>
        </ul> -->
        <ul class="col navbar-nav text-right">
            <li class="nav-item dropdown">
                <a href="#" id="user-panel" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if (session()->get('user')['picture'])
                        <span id="user-picture" style="background-image: url('/images/{{ session()->get('user')['picture'] }}')"></span>
                    @else
                        <span id="user-picture"></span>
                    @endif
                    <span id="user-name">
                        Hi, {{ session()->get('user')['first_name'] }} {{ session()->get('user')['last_name'] }}
                    </span>
                </a>
                <div id="user-panel-dropdown" class="dropdown-menu" aria-labelledby="user-panel">
                    <ul>
                        <li class="{{ request()->path() === 'profile' ? 'selected' : '' }}">
                            <a href="/profile">
                                <span class="menu-item">
                                    View Profile
                                </span>
                                <span class="menu-icon">
                                    <i class="fas fa-eye"></i>
                                </span>
                            </a>
                        </li>
                        <li class="{{ request()->path() === 'change-password' ? 'selected' : '' }}">
                            <a href="/change-password">
                                <span class="menu-item">
                                    Change Password
                                </span>
                                <span class="menu-icon">
                                    <i class="fas fa-key"></i>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="/logout">
                                <span class="menu-item">
                                    Logout
                                </span>
                                <span class="menu-icon">
                                    <i class="fas fa-sign-out-alt"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                    <ul class="mt-4">
                        <li id="developers" class="{{ request()->path() === 'developers' ? 'selected' : '' }}">
                            <hr class="mt-2 mb-2">
                            <a href="/developers">
                                <span class="menu-item">
                                    Developers
                                </span>
                                <img id="code-icon" src="/img/coding.svg" alt="Code Icon">
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</header>