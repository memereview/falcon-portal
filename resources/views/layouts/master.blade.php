<!doctype html>
<html lang="en">
<head>
    @include('layouts.meta')

    <title>{{ isset($title) ? $title . ' - ' : '' }}{{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico?">

    @include('layouts.css')
</head>
<body>
    @include('layouts.menu')

    <main>
        @include('layouts.header')

        @if (isset($breadcrumbs) && count($breadcrumbs) > 0)
            <div id="breadcrumb">
                @for ($i = 0; $i < count($breadcrumbs); $i++)
                    @if ($i < count($breadcrumbs) - 1)
                        <a href="{{ $breadcrumbs[$i][1] }}">
                            {{ $breadcrumbs[$i][0] }}
                        </a>

                        <span class="divider">
                            <i class="fas fa-angle-double-right"></i>
                        </span>
                    @else
                        <span class="last">
                            {{ $breadcrumbs[$i][0] }}
                        </span>
                    @endif
                @endfor
            </div>
        @endif

        <div id="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </main>

    @include('layouts.js')
</body>
</html>