    @extends('layouts.master', [
    'title' => 'Current Units',
    'breadcrumbs' => [
        ['Current Manifest', '/manifests/current'],
        ['Current Units', '']
    ]
])


@section('content')

    <div class="row">
        <div class="col pt-2 pb-2">       
            
            <table>
                <div class="ol pt-2 pb-2">
                    <table class="table table-hover text-center">
                        <thead>
                        <tr>
                            <th>Tracking Code</th>
                            <th>Customer Name</th>
                            <th>Customer Address</th>
                            <th>Phone Number</th>
                            <th>Email Address</th>
                            <th>Size (cm)</th>
                            <th>Weight</th>
                            <th>Payment</th>
                            <th>Status</th>
                            @if (session()->get('user')['type'] === 'ADMIN')
                                <th>Action</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($units as $unit)
                            <tr>
                                <td>{{ $unit->tracking_number }}</td>
                                <td>{{ $unit->customer->name }}</td>
                                <td>
                                    {{ $unit->address->street }}
                                    {{ $unit->address->street2 }}
                                    {{ $unit->address->barangay }}
                                    {{ $unit->address->city }},
                                    {{ $unit->address->province }}
                                    ( {{ $unit->address->postal }} )
                                </td>
                                <td>{{ $unit->address->phone }}</td>
                                <td>{{ $unit->address->email }}</td>
                                <td>
                                    {{ $unit->length }} (l)
                                    x
                                    {{ $unit->width }} (w)
                                    x
                                    {{ $unit->height }} (h)
                                </td>
                                <td>{{ $unit->weight }} kg</td>
                                <td>{{ $unit->collection }}</td>
                                <td id="unit-{{ $unit->tracking_number }}">{{ $unit->status }}</td>
                                @if (session()->get('user')['type'] === 'ADMIN')
                                    <td>
                                        <form class="delivery-verify" @if ($unit->status != 'FOR CONFIRMATION') hidden @endif>
                                            {{ csrf_field() }}

                                            <input type="hidden" name="tracking_number" value="{{ $unit->tracking_number }}">
                                            <button class="btn btn-light action view" data-toggle="tooltip" title="Verify">
                                                <i class="far fa-check-circle"></i>
                                            </button>
                                        </form>
                                        
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $units->links() }}
                </div>
            </table>
        </div>
    </div>
@endsection