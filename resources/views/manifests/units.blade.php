@extends('layouts.master', [
    'title' => 'User Details',
    'breadcrumbs' => [
        ['New Manifest', '/manifests/new'],
        ['New Units', '']
    ]
])


@section('content')

    <div class="col-sm-8">

        <h1>New units</h1>
        <br>
        <div>
            <form id="unit-verify">
                {{ csrf_field() }}
                <input type="hidden" id="manifest-number" name="manifest_number" value="{{ $manifest_number }}">

                @if (session()->get('user')['type'] === 'ADMIN')
                    <label for="tracking-number">Verify Unit:
                        <input id="tracking-number" type="text" name="tracking_number">
                    </label>    
                @endif
            </form>
            
        </div>
        <table>
            <div class="ol pt-2 pb-2">
                <table class="table table-hover text-center">
                    <thead>
                    <tr>
                        <th>Tracking Number</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                        <th>Email Address</th>
                        <th>Payment</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($units as $unit)
                        <tr>
                            <td>{{ $unit->tracking_number }}</td>
                            <td>{{ $unit->customer->name }}</td>
                            <td>
                                {{ $unit->address->street }}
                                {{ $unit->address->street2 }}
                                {{ $unit->address->barangay }}
                                {{ $unit->address->city }},
                                {{ $unit->address->province }}
                                ( {{ $unit->address->postal }} )
                            </td>
                            <td>{{ $unit->address->phone }}</td>
                            <td>{{ $unit->customer->email }}</td>
                            <td>{{ $unit->collection }}</td>
                            <td>{{ $unit->status }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </table>
    </div>
@endsection