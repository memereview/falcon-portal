@extends('layouts.master', [
    'title' => 'Archived Units',
    'breadcrumbs' => [
        ['Archived Manifest', '/manifests/archived'],
        ['Archived Units', '']
    ]
])


@section('content')

    <div class="row">
        <div class="col pt-2 pb-2">

            <br>
            
            <table>
                <div class="ol pt-2 pb-2">
                    <table class="table table-hover text-center">
                        <thead>
                        <tr>
                            <th>Tracking Code</th>
                            <th>Customer Name</th>
                            <th>Customer Address</th>
                            <th>Phone Number</th>
                            <th>Email Address</th>
                            <th>Size (cm)</th>
                            <th>Weight</th>
                            <th>Payment</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($units as $unit)
                            <tr>
                                <td>{{ $unit->tracking_number }}</td>
                                <td>{{ $unit->customer->name }}</td>
                                <td>
                                    {{ $unit->address->street }}
                                    {{ $unit->address->street2 }}
                                    {{ $unit->address->barangay }}
                                    {{ $unit->address->city }},
                                    {{ $unit->address->province }}
                                    ( {{ $unit->address->postal }} )
                                </td>
                                <td>{{ $unit->address->phone }}</td>
                                <td>{{ $unit->address->email }}</td>
                                <td>
                                    {{ $unit->length }} (l)
                                    x
                                    {{ $unit->width }} (w)
                                    x
                                    {{ $unit->height }} (h)
                                </td>
                                <td>{{ $unit->weight }} kg</td>
                                <td>{{ $unit->collection }}</td>
                                <td>{{ $unit->status }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $units->links() }}
                </div>
            </table>
        </div>
    </div>
@endsection