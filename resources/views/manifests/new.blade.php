@extends('layouts.master', [
    'title' => 'New Manifest',
    'breadcrumbs' => [
        ['New Manifest', '/manifests/new']
    ]
])

@section('content')

    <div class="row">
        <div class="col pt-2 pb-2">        
            <table>
                <div class="ol pt-2 pb-2" table-responsive>
                    <table class="table table-hover text-center">
                        <thead>
                        <tr>
                            <th>Manifest Number</th>
                            <th>Manifest Name</th>
                            <th>Company Name</th>
                            <th>Status</th>
                            <th>Upload Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($manifests as $manifest)
                                <tr>
                                    <td>{{ $manifest->number }}</td>
                                    <td>{{ $manifest->file_name }}</td>
                                    <td>{{ $manifest->client->company_name }}</td>
                                    <td>{{ $manifest->status }}</td>
                                    <td>{{ $manifest->date_created }}</td>
                                    <td>
                                        <a href="/manifests/new/units/{{$manifest->number}}" class="btn btn-light action view" data-toggle="tooltip" title="View">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr class="text-left">
                                    <td colspan="6">No Manifest Yet</td>    
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    {{ $manifests->links() }}
                </div>
            </table>
        </div>
    </div>
@endsection