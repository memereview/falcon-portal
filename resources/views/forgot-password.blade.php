<form id="forgot-password-form" method="post" action="/forgot-password" novalidate>
    @csrf

    <div class="form-title">
        <h2 class="font-weight-bold">Forgot Password</h2>
        <h6 class="font-weight-bold">Enter your email address to get instructions on how to reset your password</h6>
    </div>

    @if (isset($error_message))
        <div class="alert alert-danger" role="alert">
            {{ $error_message }}
        </div>
    @endif

    @if (isset($success_message))
        <div class="alert alert-success" role="alert">
            {{ $success_message }}
        </div>
    @endif

    <div class="form-group">
        <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" placeholder="Email Address" required>
        <div class="invalid-feedback">
            @if ($errors->has('email'))
                {{ $errors->get('email')[0] }}
            @endif
        </div>
    </div>

    <div>
        <button id="back-to-login" class="btn btn-link" type="button">Back to Login</button>
    </div>

    <button class="btn">Reset Password</button>
</form>