@extends('layouts.master', [
	'title' => 'User Details',
	'breadcrumbs' => [
		['Manage Accounts', '/admin/users'],
		[$user->first_name . ' ' . $user->last_name, '/admin/users/' . $user->id]
	]
])

@section('content')
	
	<div class="row">
		<div class="col pt-2 pb-2">

			<h1>user Details</h1>

			<br>
			<p><b>First Name:</b> {{ $user->first_name }}</p>
			<p><b>Middle Name:</b> {{ $user->middle_name }}</p>
			<p><b>Last Name:</b> {{ $user->last_name }}</p>
			<p><b>Email:</b> {{ $user->email }}</p>
			<p>
				<b>User Type:</b>
					@if ($user->type == 'CLIENT')
						SUPPLIER
					@else
						{{ $user->type }}
					@endif
			</p>
			
			@if ($user->type === "CLIENT")
				<p><b>Company Name:</b> {{ $user->company_name }}</p>
				<p><b>Address:</b>
					{{ $clients->street }}
	                {{ $clients->street2 }}
	                {{ $clients->barangay }}
	                {{ $clients->city }},
	                {{ $clients->province }}
	                ({{ $clients->postal }})
				</p>
			@endif
			<br>
			<form action="/admin/users/{{$user->id}}/edit" method="GET">
				<button class="btn btn-primary">Edit Account</button>
			</form>
			<br>
			</div>
		</div>
	</div>
@endsection