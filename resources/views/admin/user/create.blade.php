@extends('layouts.master', [
    'title' => 'create User',
    'breadcrumbs' => [
        ['Manage Accounts', '/admin/users'],
        ['Create User', '/admin/users/create'],
    ]
])

@section('content')
    <div class="col-sm-8">
    	<br>
		<form method="POST" action="/admin/users" novalidate>
			@csrf

			<div class="form-group">

				<label for="first_name" class="required">First Name:</label>

				@if ($errors->has('first_name'))
					<div class="alert alert-danger" role="alert" style="width: 50%">
						{{ $errors->get('first_name')[0] }}
					</div>
				@endif

				<input type="text" class="form-control" id="first_name"
					   name="first_name" value="{{ old('first_name') }}" required>

			</div>

			<div class="form-group">

				<label for="middle_name">Middle Name:</label>

				@if ($errors->has('middle_name'))
					<div class="alert alert-danger" role="alert" style="width: 50%">
						{{ $errors->get('middle_name')[0] }}
					</div>
				@endif

				<input type="text" class="form-control" id="middle_name"
					   name="middle_name" value="{{ old('middle_name') }}">



			</div>

			<div class="form-group">

				<label for="last_name" class="required">Last Name:</label>

				@if ($errors->has('last_name'))
					<div class="alert alert-danger" role="alert" style="width: 50%">
						{{ $errors->get('last_name')[0] }}
					</div>
				@endif

				<input type="text" class="form-control" id="last_name"
					   name="last_name" value="{{ old('last_name') }}" required>

			</div>

			<div class="form-group">

				<label for="email" class="required">Email:</label>

				@if ($errors->has('email'))
					<div class="alert alert-danger" role="alert" style="width: 50%">
						{{ $errors->get('email')[0] }}
					</div>
				@endif

				<input type="email" class="form-control" id="email"
					   name="email" value="{{ old('email') }}" required>

			</div>

			<div class="form-group">

				<label for="type" class="required">User Type:</label>

				<select name="type" class="form-control" id="type" required>
					<option value="ADMIN" {{ old('type') == 'ADMIN' ? 'selected' : '' }}>Admin</option>
					<option value="CLIENT" {{ old('type') == 'CLIENT' ? 'selected' : '' }}>Supplier</option>
					<option value="RIDER" {{ old('type') == 'RIDER' ? 'selected' : '' }}>Rider</option>
				</select> 

			</div>

			<div id="company" @if(old('type') === 'CLIENT') style="display: block" @endif>
                <div class="form-group">
                    <label for="company_name" class="required">Company Name:</label>

					@if ($errors->has('company_name'))
						<div class="alert alert-danger" role="alert" style="width: 50%">
							{{ $errors->get('company_name')[0] }}
						</div>
					@endif

                    <input type="text" class="form-control" id="company_name"
                           name="company_name" value="{{ old('company_name') }}" required>

                </div>

                <div class="form-group">
                    <label for="street_1" class="required">Street Address 1:</label>

					@if ($errors->has('street_1'))
						<div class="alert alert-danger" role="alert" style="width: 50%">
							{{ $errors->get('street_1')[0] }}
						</div>
					@endif

                    <input type="text" class="form-control" id="street_1"
                           name="street_1" value="{{ old('street_1') }}" required>

                </div>

                <div class="form-group">
                    <label for="street_2">Street Address 2:</label>

					@if ($errors->has('street_2'))
						<div class="alert alert-danger" role="alert" style="width: 50%">
							{{ $errors->get('street_2')[0] }}
						</div>
					@endif

                    <input type="text" class="form-control" id="street_2"
                           name="street_2" value="{{ old('street_2') }}">

                </div>

                <div class="form-group">
                    <label for="barangay">Barangay:</label>

					@if ($errors->has('barangay'))
						<div class="alert alert-danger" role="alert" style="width: 50%">
							{{ $errors->get('barangay')[0] }}
						</div>
					@endif

                    <input type="text" class="form-control" id="barangay"
                           name="barangay" value="{{ old('barangay') }}">

                </div>

                <div class="form-group">
                    <label for="city" class="required">City:</label>

					@if ($errors->has('city'))
						<div class="alert alert-danger" role="alert" style="width: 50%">
							{{ $errors->get('city')[0] }}
						</div>
					@endif

                    <input type="text" class="form-control" id="city"
                           name="city" value="{{ old('city') }}" required>

                </div>

                <div class="form-group">
                    <label for="province" class="required">Province:</label>

					@if ($errors->has('province'))
						<div class="alert alert-danger" role="alert" style="width: 50%">
							{{ $errors->get('province')[0] }}
						</div>
					@endif

                    <input type="text" class="form-control" id="province"
                           name="province" value="{{ old('province') }}" required>

                </div>

                <div class="form-group">
                    <label for="postal_code" class="required">Postal Code:</label>

					@if ($errors->has('postal_code'))
						<div class="alert alert-danger" role="alert" style="width: 50%">
							{{ $errors->get('postal_code')[0] }}
						</div>
					@endif

                    <input type="text" class="form-control" id="postal_code"
                           name="postal_code" value="{{ old('postal_code') }}" required>

                </div>

			</div>
            <br>
			<div class="form-group">
					<button type="submit" class="btn btn-primary">Create</button>
			</div>
		</form>
        <br>
	</div>
@endsection
