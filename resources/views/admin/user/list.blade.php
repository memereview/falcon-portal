@extends('layouts.master', [
	'title' => 'User List',
	'breadcrumbs' => [
		['Manage Accounts', '/admin/users']
	]
])

@section('content')

	<div class="row">
		<div class="col pt-2 pb-2">
			<div class="action">
				<a href="/admin/users/create" class="btn btn-primary">
					Create new User
				</a>
			</div>

			<table class="table table-hover text-center">
				<thead>
				<tr>
					<th></th>
					<th>First Name</th>
					<th>Middle Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>User Type</th>
					<th>Activated</th>
					<th>Last Login</th>
					<th>Action<th>
				</tr>
				</thead>
				<tbody>
				@foreach($users as $user)
					<tr>
						@if ($user->picture)
							<td><img id="user-picture" src="/images/{{ $user->picture }}"></td>
						@else
							<td><img id="user-picture" src="/img/default-profile-picture.jpg"></td>
						@endif
						<td>{{ $user->first_name }}</td>
						<td>{{ $user->middle_name }}</td>
						<td>{{ $user->last_name }}</td>
						<td>{{ $user->email }}</td>
						<td>
							@if ($user->type === 'ADMIN')
								<span class="badge badge-pill badge-success">
									{{ ucwords(strtolower($user->type)) }}
								</span>
							@elseif($user->type === 'CLIENT')
								<span class="badge badge-pill badge-primary">
									{{--{{ ucwords(strtolower($user->type)) }}--}}
									Supplier
								</span>
							@else
								<span class="badge badge-pill badge-secondary">
									{{ ucwords(strtolower($user->type)) }}
								</span>
							@endif
						</td>
						<td>{{ $user->verified ? 'Yes' : 'No' }}</td>
						<td>
							
							@isset($user->date_login)
								{{ $user->date_login }}
							@endisset
							@empty($user->date_login)
								No first login yet
							@endempty
						</td>
						<td>

							<a href="/admin/users/{{$user->id}}" class="btn btn-light action view"
									data-toggle="tooltip" title="View">
								<i class="fas fa-eye"></i>
							</a>

							<a href="/admin/users/{{$user->id}}/edit" class="btn btn-light action edit"
									data-toggle="tooltip" title="Edit">
								<i class="fas fa-pencil-alt"></i>
							</a>

                            <button class="btn btn-light action delete"
                                    data-delete-id="{{$user->id}}"
                                    data-toggle="tooltip" title="Delete">
                                <i class="fas fa-trash-alt"></i>
                            </button>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
			{{ $users->links() }}
		</div>
	</div>

@endsection