@extends('layouts.master', [
    'title' => 'Assign Units',
    'breadcrumbs' => [
        ['Assign Units', '/admin/assign-units']
    ]
])

@section('content')
    <div class="row">
        <div class="col pt-2 pb-2">
            <br>
            <button id="auto-assign" class="btn btn-primary">Auto-assign</button>
            <button id="clear" class="btn btn-primary">Remove all</button>

            <div class="center-assign">
                <br>
                <p id="verified-units"><b>VERIFIED UNITS</b></p>
                <div id="units" class="list-group-unit" style="min-height: 300px;">
                    @foreach($units as $unit)
                        <div class="dragged list-group-item" draggable="true" data-unit="{{ $unit->id }}" id="unit{{ $unit->id }}">
                            <p>
                                {{ $unit->tracking_number }}
                                <br>
                                {{ $unit->customer->name }}
                                <br>
                                {{ $unit->address->street }}
                                {{ $unit->address->street2 }}
                                {{ $unit->address->barangay }}
                                <br>
                                {{ $unit->address->city }},
                                {{ $unit->address->province }}
                                ( {{ $unit->address->postal }} )
                            </p>
                        </div>
                    @endforeach
                </div>
            </div>
            
            <form id="assign-units">
                @csrf
                <div class=rider-column>
                    @foreach($riders as $rider)
                    <div>
                        <div class="rider-name">
                            <p id="rider-name"><b>{{ $rider->first_name }}  {{ $rider->last_name }}</b></p>
                        </div>
                        <div class="assigned-container">
                            <div class="riders" id="rider{{ $rider->id }}" data-rider="{{ $rider->id }}" style="min-height: 300px;">
                            <!-- Loop assigned units of rider here -->
                            @foreach($deliveries as $delivery)
                                @if($delivery->rider_id == $rider->id)
                                    @if($delivery->status != 'IN TRANSIT')
                                        <div class="dragged list-group-item" draggable="true" data-unit="{{ $delivery->unit_id }}">
                                            <p>
                                                {{ $delivery->tracking_number }}
                                                <br>
                                                {{ $delivery->name }}
                                                <br>
                                                {{ $delivery->street }}
                                                {{ $delivery->street2 }}
                                                {{ $delivery->barangay }}
                                               <br>
                                                {{ $delivery->city }},
                                                {{ $delivery->province }}
                                                ( {{ $delivery->postal }} )
                                            </p>
                                       </div>
                                    @else
                                        <div class="undragged list-group-item" draggable="false" data-unit="{{ $delivery->unit_id }}">
                                            <p>
                                                {{ $delivery->tracking_number }}
                                                <br>
                                                {{ $delivery->name }}
                                                <br>
                                                {{ $delivery->street }}
                                                {{ $delivery->street2 }}
                                                {{ $delivery->barangay }}
                                               <br>
                                                {{ $delivery->city }},
                                                {{ $delivery->province }}
                                                ( {{ $delivery->postal }} )
                                            </p>
                                        </div>
                                    @endif
                                @endif
                            @endforeach
                           </div>
                        </div>  
                    </div>
                    @endforeach
                </div>
                <input type="submit" value="Assign" class="btn btn-primary">
            </form>
            <br>
        </div>
    </div>
@endsection



                    
               