@extends('layouts.master', [
    'title' => 'Developers',
    'breadcrumbs' => [
        ['Developers', '/developers']
    ]
])

@section('content')
    <div class="col-sm-8">

        <br>
        <br>
        
        <p><b>Sherwin Niko A. Okuyama</b></p>
        <p><b>10000311100</b></p>
        <p><b>BSCS</b></p>
        <p><b>sn.okuyama@gmail.com</b></p>
        <p><b>0956 922 8017</b></p>
        
        <br>
        <br>
        
        <p><b>Jahn Austine Daniel Y. De Vera</b></p>
        <p><b>10000310700</b></p>
        <p><b>BSCS</b></p>
        <p><b>austinedevera@gmail.com</b></p>
        <p><b>0917 631 4418</b></p>
    
        <br>
               
    </div>
@endsection