<!doctype html>
<html lang="en">
<head>
    @include('layouts.meta')

    <title>Login - {{ config('app.name', 'Falcon') }}</title>

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

    @include('layouts.css')
</head>
<body>
    
    <div id="login-right" class="row public-form">
        <div id="login-form-wrapper" class="col-md-12">
            <form id="login-form" method="post" action="/login" novalidate>
                @csrf

                <div class="form-title">
                    <h2 class="font-weight-bold">Log In</h2>
                    <h6 class="font-weight-bold">Enter your credentials to access the {{ config('app.name', 'Falcon') }} portal</h6>
                </div>

                @if (isset($error_message))
                    <div class="alert alert-danger" role="alert">
                        {{ $error_message }}
                    </div>
                @endif

                @if (isset($success_message))
                    <div class="alert alert-success" role="alert">
                        {{ $success_message }}
                    </div>
                @endif

                <div class="form-group">
                    <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" placeholder="Email Address" required>
                    <div class="invalid-feedback">
                        @if ($errors->has('email'))
                            {{ $errors->get('email')[0] }}
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" placeholder="Password" required>
                    <div class="invalid-feedback">
                        @if ($errors->has('password'))
                            {{ $errors->get('password')[0] }}
                        @endif
                    </div>
                </div>

                <div>
                    <button id="forgot-password" class="btn btn-link" type="button">Forgot your Password?</button>
                </div>

                <button class="btn">Log In</button>
            </form>

            @include('forgot-password')
        </div>
    </div>

    <div id="login-left">
        <div id="login-message">
            <div class="row">
                <div class="col-8 offset-4">
                    <h2 class="text-white font-weight-bold">Welcome to {{ config('app.name', 'Falcon') }}!</h2>
                    <h6 class="text-white font-weight-bold">Hey there! Using the {{ config('app.name'), 'Falcon' }} app, you can upload your manifest, assign the deliveries to the riders, track your deliveries and so much more!</h6>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.js')

</body>
</html>