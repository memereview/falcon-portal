<!doctype html>
<html lang="en">
<head>
    @include('layouts.meta')

    <title>Delivery Confirmation - {{ config('app.name', 'Falcon') }}</title>

    @include('layouts.css')
</head>
<body>
<div class="verify-header">
    <a href="/login">
        <img src="/img/logo.png">
        <span id="app-name-main">
                {{ config('app.name', 'Falcon') }}
            </span>
    </a>
</div>
<br>
<br>


    <div class="p-4 row">

        <div id="login-form-wrapper" class="col">
            <div id="login-title" style="margin-left: 450px;">
                <h2>Your response has been processed</h2>
                <p><b>Thank you for trusting falcon!</b></p>
            </div>
        </div>

    </div>


@include('layouts.js')

</body>
</html>


