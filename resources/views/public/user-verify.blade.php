<!doctype html>
<html lang="en">
<head>
    @include('layouts.meta')

    <title>Account Activation - {{ config('app.name', 'Falcon') }}</title>

    @include('layouts.css')
</head>
<body>
    <div class="verify-header">
        <a href="/login">
            <img src="/img/logo.png">
            <span id="app-name-main">
                {{ config('app.name', 'Falcon') }}
            </span>
        </a>
    </div>
    <br>
    <br>
    <form action="/verify/{{ $token }}" method="post" novalidate>
        @csrf

        <div class="p-4 row">

            <div id="login-form-wrapper" class="col">
                <div id="login-title" style="margin-left: 450px;">
                    <h2>Account Activation</h2>
                    <p><b>Enter and confirm your first password to activate your account.</b></p>

                    <div class="form-group">
                        <h5 class="font-weight-bold">Enter First Password:</h5>
                        <label for="password">
                            <input id="password" type="password" name="password" class="form-control">
                            @if ($errors->has('password'))
                                <span>
                                    {{ $errors->get('password')[0] }}
                                </span>
                            @endif
                        </label>
                    </div>

                    <div class="form-group">
                        <h5 class="font-weight-bold">Confirm Password:</h5>
                        <label for="password_confirmation">
                            <input id="password_confirmation" type="password" name="password_confirmation" class="form-control">
                            @if ($errors->has('password_confirmation'))
                                <span>
                                    {{ $errors->get('password_confirmation')[0] }}
                                </span>
                            @endif
                        </label>
                    </div>
                    <div class="form-group">
                        <p><b>Passwords must be at least 6 characters long.</b></p>
                    </div>
                    <button id="login-button" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </form>

@include('layouts.js')

</body>
</html>


