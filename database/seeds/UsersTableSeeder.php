<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Admin',
            'middle_name' => 'Admin',
            'last_name' => 'Admin',
            'email' => 'admin@falcon.com',
            'password' => bcrypt('admin'),
            'type' => 'ADMIN',
            'verified' => 1
        ]);

        DB::table('users')->insert([
            'first_name' => 'Client',
            'middle_name' => 'Client',
            'last_name' => 'Client',
            'email' => 'client@falcon.com',
            'password' => bcrypt('client'),
            'type' => 'CLIENT',
            'verified' => 1
        ]);

        DB::table('clients')->insert([
            'user_id' => 2,
            'company_name' => 'Lazada',
            'street' => 'Phase 3 Block 13',
            'street2' => 'Burgos Street',
            'barangay' => 'San Vicente',
            'city'  => 'San Pedro',
            'province' => 'Laguna',
            'postal' => '4023',
            'country' => 'PH'
        ]);

        DB::table('users')->insert([
            'first_name' => 'Client2',
            'middle_name' => 'Client2',
            'last_name' => 'Client2',
            'email' => 'client2@falcon.com',
            'password' => bcrypt('client'),
            'type' => 'CLIENT',
            'verified' => 1
        ]);

        DB::table('clients')->insert([
            'user_id' => 3,
            'company_name' => 'Jollibee',
            'street' => 'Phase 3 Block 13',
            'street2' => 'Burgos Street',
            'barangay' => 'San Vicente',
            'city'  => 'San Pedro',
            'province' => 'Laguna',
            'postal' => '4023',
            'country' => 'PH'
        ]);

        DB::table('users')->insert([
            'first_name' => 'Noctis',
            'middle_name' => 'Lucius',
            'last_name' => 'Calem',
            'email' => 'rider1@falcon.com',
            'password' => bcrypt('rider'),
            'type' => 'RIDER',
            'verified' => 1
        ]);

        DB::table('users')->insert([
            'first_name' => 'Jamarus',
            'middle_name' => 'Jamarison',
            'last_name' => 'Lamar',
            'email' => 'rider2@falcon.com',
            'password' => bcrypt('rider'),
            'type' => 'RIDER',
            'verified' => 1
        ]);

        DB::table('users')->insert([
            'first_name' => 'Keygan',
            'middle_name' => 'Michael',
            'last_name' => 'Peele',
            'email' => 'rider3@falcon.com',
            'password' => bcrypt('rider'),
            'type' => 'RIDER',
            'verified' => 1
        ]);


    }
}
