<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Manifests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manifests', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->string('file_name', 255);
            $table->bigInteger('number')->index()->unique()->unsigned();
            $table->enum('status', ['NEW', 'RECEIVED', 'COMPLETED']);
            $table->timestamp('date_created')
                ->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('date_updated')
                ->default(DB::raw(
                    'CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
                
            $table->foreign('client_id')
                ->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manifests');
    }
}
