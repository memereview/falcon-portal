<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Units extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->integer('address_id')->unsigned();
            $table->bigInteger('manifest_number')->unsigned();
            $table->string('tracking_number', 100)->index();
            $table->float('length')->unsigned();
            $table->float('width')->unsigned();
            $table->float('height')->unsigned();
            $table->float('weight')->unsigned();
            $table->float('collection', 10, 2)->unsigned();
            $table->enum('status', ['NEW', 'VERIFIED', 'DELIVERED', 'CANCELLED', 'IN TRANSIT', 'FOR CONFIRMATION', 'FAILED']);
            $table->longText('remarks');
            $table->integer('client_id')->unsigned();
            $table->timestamp('date_created')
                ->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('date_updated')
                ->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));

            $table->foreign('customer_id')
                ->references('id')->on('customers');
            $table->foreign('address_id')
                ->references('id')->on('customer_addresses');
            $table->foreign('manifest_number')
                ->references('number')->on('manifests');
            $table->foreign('client_id')
                ->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
