<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReturnedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('returned', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('issue');
            $table->string('remarks', 50);
            $table->integer('unit_id')->unsigned();
            $table->timestamp('date_created')
                ->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('unit_id')
                ->references('id')->on('units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('returned');
    }
}
