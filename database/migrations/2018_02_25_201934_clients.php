<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Clients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('company_name', 255);
            $table->string('street', 255);
            $table->string('street2', 255)->nullable();
            $table->string('barangay', 255)->nullable();
            $table->string('city', 255);
            $table->string('province', 255);
            $table->string('postal', 10);
            $table->enum('country', ['PH']);
            $table->timestamp('date_created')
                ->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('date_updated')
                ->default(DB::raw(
                    'CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
                
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
