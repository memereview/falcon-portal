<?php

namespace App\Mail;

use App\Models\User;
use App\Models\Manifest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailUploaded extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Instance of the User
     *
     * @var User
     */
    public $user;

    /**
     * Instance of the Manifest
     *
     * @var Manifest
     */
    public $filename;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param $filename
     */
    public function __construct(User $user, $filename)
    {
        $this->user = $user;
        $this->filename = $filename;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.uploaded')
            ->subject('A Manifest has been successfully uploaded');
    }
}

