<?php

namespace App\Mail;

use App\Models\User;
use App\Models\Manifest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailAdminDelivered extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Instance of the User
     *
     * @var User
     */
    public $user;

    /**
     * Instance of the Manifest
     *
     * @var Manifest
     */
    public $manifest;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param Manifest $manifest
     */
    public function __construct(User $user, Manifest $manifest)
    {
        $this->user = $user;
        $this->manifest = $manifest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.admin-delivered')
            ->subject('All units were successfully delivered!');
    }
}

