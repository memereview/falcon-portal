<?php

namespace App\Mail;

use App\Models\Client;
use App\Models\Manifest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailClientDelivered extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Instance of the Client
     *
     * @var Client
     */
    public $client;

    /**
     * Instance of the Manifest
     *
     * @var Manifest
     */
    public $manifest;

    /**
     * Create a new message instance.
     *
     * @param Client $client
     * @param Manifest $manifest
     */
    public function __construct(Client $client, Manifest $manifest)
    {
        $this->client = $client;
        $this->manifest = $manifest;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.client-delivered')
            ->subject('All units were successfully delivered!');
    }
}
