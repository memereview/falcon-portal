<?php

namespace App\Mail;

use App\Models\Unit;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailTransit extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Instance of the customer unit
     *
     * @var User
     */
    public $unit;


    /**
     * Create a new message instance.
     *
     * @param Unit $unit
     */
    public function __construct(Unit $unit)
    {
        $this->unit = $unit;


//        $this->url = \Config::get('app.url') . '/reset-password/' .
//            $resetPassword->token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.transit')
            ->subject('Your Delivery is on the way!');
    }
}
