<?php

namespace App\Mail;

use App\Models\Unit;
use App\Models\DeliveryConfirmation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailDelivered extends Mailable
{
    use Queueable, SerializesModels;

    public $unit;

    public $confirmation;

    public $urlDelivered;

    public $urlFailed;

    public function __construct(Unit $unit, DeliveryConfirmation $confirmation)
    {
        $this->unit = $unit;
        $this->confirmation = $confirmation;

        $this->urlDelivered = \Config::get('app.url') . '/delivered/' .
            $confirmation->token;

        $this->urlFailed = \Config::get('app.url') . '/failed/' .
            $confirmation->token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.delivered')
            ->subject('Please confirm delivery');
    }
}
