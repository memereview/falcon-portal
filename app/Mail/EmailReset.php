<?php

namespace App\Mail;

use App\Models\User;
use App\Models\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailReset extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Instance of the user
     *
     * @var User
     */
    public $user;

    /**
     * Instance of the reset password
     *
     * @var User
     */
    public $resetPassword;

    /**
     * Generated url
     *
     * @var string
     */
    public $url;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param ResetPassword $resetPassword
     */
    public function __construct(User $user, ResetPassword $resetPassword)
    {
        $this->user = $user;
        $this->resetPassword = $resetPassword;

        $this->url = \Config::get('app.url') . '/reset-password/' .
            $resetPassword->token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.reset-password')
            ->subject('Reset Password');
    }
}
