<?php

namespace App\Mail;

use App\Models\User;
use App\Models\UserVerification;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailVerify extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Instance of the user
     * 
     * @var User
     */
    public $user;

    /**
     * Instance of the user verification
     *
     * @var User
     */
    public $userVerification;

    /**
     * Generated url
     *
     * @var string
     */
    public $url;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param UserVerification $userVerification
     */
    public function __construct(User $user, UserVerification $userVerification)
    {
        $this->user = $user;
        $this->userVerification = $userVerification;

        $this->url = \Config::get('app.url') . '/verify/' . $userVerification->token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user-verification')
            ->subject('Welcome to Falcon!');
    }
}
