<?php

namespace App\Http\Controllers;

use App\Models\User;
use DateTime;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class ImageController extends Controller
{

    /**
     * View the image
     *
     * @param $image
     * @return mixed
     */
    public function index($image)
    {
        // file path for the image
        $path = storage_path('images' . DIRECTORY_SEPARATOR . 'profile') . DIRECTORY_SEPARATOR . $image;

        // check if the file exists. if not, return 404 and a null content
        if (!File::exists($path)) {
            return Response::make(null, 404);
        }

        // get the file
        $file = File::get($path);
        // get the mime type
        $type = File::mimeType($path);

        // create a response, also add the header using the mime type
        $response = Response::make($file, 200);
        $response->header('Content-Type', $type);

        return $response;
    }

    /**
     * Upload the profile picture to the server
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadProfilePicture()
    {
        $sessionUser = session('user');
        $file = request()->file('picture');
        $allowedMimes = [
            'image/jpeg',
            'image/jpg',
            'image/png',
            'image/gif'
        ];

        if (!$file || !isset($file) || empty($file)) {
            return response()->json([
                'error'     => true,
                'response'  => 'The picture is required'
            ]);
        }

        if (!in_array($file->getClientMimeType(), $allowedMimes)) {
            return response()->json([
                'error'     => true,
                'response'  => 'Invalid image type'
            ]);
        }

        // create the filename for the uploaded picture. this should be unique
        $fileName = strtoupper(md5($sessionUser['email'])) . '_' . rand(1000000000, 2147483647) . '_' .
            date_format(new DateTime(), 'Ymd_His') . '.' . $file->getClientOriginalExtension();
        $folderPath = storage_path('images' . DIRECTORY_SEPARATOR . 'profile');

        // move the file to the server
        $file->move($folderPath, $fileName);

        // get the user's data
        $user = User::find($sessionUser['id']);

        // if the user has a previous picture, delete it first
        if ($user->picture) {
            File::delete($folderPath . DIRECTORY_SEPARATOR . $user->picture);
        }

        // update the user's picture
        $user->picture = $fileName;

        // update the user
        $user->save();

        // update the session
        session(['user' => $user->fieldsForSession()]);

        return response()->json([
            'error'     => false,
            'response'  => $fileName
        ]);
    }

}