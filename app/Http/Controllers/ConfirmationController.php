<?php
/**
 * Created by PhpStorm.
 * User: Sherwin
 * Date: 4/10/2018
 * Time: 4:41 AM
 */

namespace App\Http\Controllers;

use App\Mail\EmailClientDelivered;
use App\Mail\EmailAdminDelivered;
use App\Models\DeliveryConfirmation;
use App\Models\Unit;
use App\Models\User;
use App\Models\Client;
use App\Models\Manifest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ConfirmationController extends Controller
{
    public function delivered($token)
    {
        $tokenData = DeliveryConfirmation::where('token', $token)->first();

        //check if token exists
        if (!$tokenData) {
            return redirect('/expired');
        }

        // get unit data
        $unit = Unit::where([
            ['id', $tokenData->unit_id],
            ['status', Unit::STATUS_CONFIRMATION]
        ])->first();

        //update unit data
        $unit->status = Unit::STATUS_DELIVERED;
        $unit->save();

        //delete token
        DeliveryConfirmation::where('token', $token)->delete();

        //Get manifest number of tracking number
        $manifestNumber = Unit::where('tracking_number', $unit->tracking_number)->first()->manifest_number;

        //Count total units of manifest
        $totalUnits = Unit::where('manifest_number', $manifestNumber)->count();

        //Get all units with delivered status and count
        $deliveredUnits = DB::table('units')
            ->where('units.status', 'DELIVERED')
            ->where('units.manifest_number', $manifestNumber)
            ->get();
        $totalDelivered = count($deliveredUnits);

        if ($totalUnits == $totalDelivered) {
            // update the manifest status to completed
            DB::table('manifests')
                ->where('manifests.number', $manifestNumber)
                ->update(['status' => Manifest::STATUS_COMPLETED]);

            // send email to the client that uploaded the manifest
            $manifest = Manifest::has('client')->where('number', $manifestNumber)->first();
            $client = Client::has('user')->where('id', $manifest->client->id)->first();

            $emailClientDelivered = new EmailClientDelivered($client, $manifest);

            if (config('app.env') === 'production') {
                Mail::to($client->user->email)->queue($emailClientDelivered);
            } else {
                Mail::to($client->user->email)->send($emailClientDelivered);
            }

            $admins = User::where('type', 'admin')->get();

            foreach ($admins as $admin) {

            $emailAdminDelivered = new EmailAdminDelivered($admin, $manifest);

                if (config('app.env') === 'production') {
                    Mail::to($admin->email)->queue($emailAdminDelivered);
                } else {
                    Mail::to($admin->email)->send($emailAdminDelivered);
                }
            }
        }

        return view('/public/confirmation',  ['token' => $token]);
    }

    public function failed($token)
    {
        $tokenData = DeliveryConfirmation::where('token', $token)->first();

        //check if token exists
        if (!$tokenData) {
            return redirect('/expired');
        }

        // get unit data
        $unit = Unit::where([
            ['address_id', $tokenData->address_id],
            ['status', Unit::STATUS_CONFIRMATION]
        ])->first();

        //update unit data
        $unit->status = Unit::STATUS_FAILED;
        $unit->save();

        //delete token
        DeliveryConfirmation::where('token', $token)->delete();

        return view('/public/confirmation',  ['token' => $token]);
    }

    public function expired()
    {
        return view('/public/expired');
    }

    public function verify($trackingNumber)
    {
        $unit = Unit::where('tracking_number', $trackingNumber)->first();

        //update unit data
        $unit->status = Unit::STATUS_DELIVERED;
        $unit->save();

        //Get manifest number of tracking number
        $manifestNumber = $unit->manifest_number;

        //Count total units of manifest
        $totalUnits = Unit::where('manifest_number', $manifestNumber)->count();

        //Get all units with delivered status and count
        $deliveredUnits = DB::table('units')
            ->where('units.status', 'DELIVERED')
            ->where('units.manifest_number', $manifestNumber)
            ->get();

        $totalDelivered = count($deliveredUnits);

        if ($totalUnits == $totalDelivered) {
            // update the manifest status to completed
            DB::table('manifests')
                ->where('manifests.number', $manifestNumber)
                ->update(['status' => Manifest::STATUS_COMPLETED]);

            // send email to the client that uploaded the manifest
            $manifest = Manifest::has('client')->where('number', $manifestNumber)->first();
            $client = Client::has('user')->where('id', $manifest->client->id)->first();

            $emailClientDelivered = new EmailClientDelivered($client, $manifest);

            if (config('app.env') === 'production') {
                Mail::to($client->user->email)->queue($emailClientDelivered);
            } else {
                Mail::to($client->user->email)->send($emailClientDelivered);
            }

            $admins = User::where('type', 'admin')->get();

            foreach ($admins as $admin) {

            $emailAdminDelivered = new EmailAdminDelivered($admin, $manifest);

                if (config('app.env') === 'production') {
                    Mail::to($admin->email)->queue($emailAdminDelivered);
                } else {
                    Mail::to($admin->email)->send($emailAdminDelivered);
                }
            }
        }

        return response()->json([
            'error'     => false,
            'response'  => [
                'unitVerified' => 'The delivery has been confirmed',
                'checkManifest' => true,
            ]
        ]);         
    }
}