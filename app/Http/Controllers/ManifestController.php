<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Manifest;
use App\Models\Unit;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ManifestController extends Controller
{
    public function index()
    {
        return view('/manifests');
    }

    public function showNewUnits($number)
    {

        $user = session()->get('user');
        $userId = $user['id'];
        $manifest = Manifest::has('client')->where('number', $number)->get()->first();

        if(!$manifest) {
            return view('/404');
        } else {
            $clientId = $manifest->client->user_id;
        }

        $units = Unit::has('address')->where('manifest_number', '=', $number)->paginate(10);

        foreach ($units as $unit) {
            $manifest_number = $unit->manifest_number;
        }

        if (session()->get('user')['type'] == User::TYPE_CLIENT) {
            if ($userId !== $clientId) {
                return redirect()->action('ForbiddenController@index');
            } else {
                return view('/manifests/new-units', compact('units', 'manifest_number'));
            }
        } else {
            return view('/manifests/new-units', compact('units', 'manifest_number'));
        }
    }

    public function showCurrentUnits($number)
    {

        $user = session()->get('user');
        $userId = $user['id'];
        $manifest = Manifest::has('client')->where('number', $number)->get()->first();

        if(!$manifest) {
            return view('/404');
        } else {
            $clientId = $manifest->client->user_id;
        }

        $units = Unit::has('address')->where('manifest_number', '=', $number)->paginate(10);

        foreach ($units as $unit) {
            $manifest_number = $unit->manifest_number;
        }

        if (session()->get('user')['type'] == User::TYPE_CLIENT) {
            if ($userId !== $clientId) {
                return redirect()->action('ForbiddenController@index');
            } else {
                return view('/manifests/current-units', compact('units', 'manifest_number'));
            }
        } else {
            return view('/manifests/current-units', compact('units', 'manifest_number'));
        }
    }


    public function showArchivedUnits($number)
    {

        $user = session()->get('user');
        $userId = $user['id'];
        $manifest = Manifest::has('client')->where('number', $number)->get()->first();

        if(!$manifest) {
            return view('/404');
        } else {
            $clientId = $manifest->client->user_id;
        }

        $units = Unit::has('address')->where('manifest_number', '=', $number)->paginate(10);

        foreach ($units as $unit) {
            $manifest_number = $unit->manifest_number;
        }

        if (session()->get('user')['type'] == User::TYPE_CLIENT) {
            if ($userId !== $clientId) {
                return redirect()->action('ForbiddenController@index');
            } else {
                return view('/manifests/archived-units', compact('units', 'manifest_number'));
            }
        } else {
            return view('/manifests/archived-units', compact('units', 'manifest_number'));
        }
    }

    public function showNew()
    {
        $manifests = Manifest::where('status', 'NEW')->paginate(10);

        if (session()->get('user')['type'] == User::TYPE_CLIENT) {
            $user = session()->get('user');
            $userId = $user['id'];
            $client = Client::where('user_id', $userId)->first();
            $clientId = $client->id;

            $manifests = Manifest::where([
                ['status', '=', 'NEW'],
                ['client_id', '=', $clientId]
            ])->paginate(10);
        }

        return view('/manifests/new', compact('manifests'));
    }

    public function showCurrent()
    {
        $manifests = Manifest::where('status', 'RECEIVED')->paginate(10);

        if (session()->get('user')['type'] == User::TYPE_CLIENT) {
            $user = session()->get('user');
            $userId = $user['id'];
            $client = Client::where('user_id', $userId)->first();
            $clientId = $client->id;

            $manifests = Manifest::where([
                ['status', '=', 'RECEIVED'],
                ['client_id', '=', $clientId]
            ])->paginate(10);
        }

        return view('/manifests/current', compact('manifests'));
    }

    public function showArchives()
    {
        $manifests = Manifest::where('status', 'COMPLETED')->paginate(10);

        if (session()->get('user')['type'] == User::TYPE_CLIENT) {
            $user = session()->get('user');
            $userId = $user['id'];
            $client = Client::where('user_id', $userId)->first();
            $clientId = $client->id;

            $manifests = Manifest::where([
                ['status', '=', 'COMPLETED'],
                ['client_id', '=', $clientId]
            ])->paginate(10);
        }

        return view('/manifests/archived', compact('manifests'));
    }

    public function verifyUnit(Request $request)
    {
        $trackingNumber = request()->trackingNumber;
        $manifestNumber = request()->manifestNumber;

        $verified = "VERIFIED";

        $unit = DB::table('units')->select(DB::raw('units.*'))
            ->where('tracking_number', $trackingNumber)
            ->first();
  
        if(!$unit){
            return response()->json([
                'error'     => false,
                'response'  => [
                        'unitVerified' => 'No tracking number matched.',
                        'checkManifest' => false
                    ]
            ]);
        }
        else{
            $getUnit = Unit::find($unit->id);
            $getUnit->status = $verified;
            $getUnit->save();

            $totalUnits = Unit::where('manifest_number', $manifestNumber)->count();

            $totalVerifiedUnits = DB::table('units')->where([
                ['status', '=', $verified],
                ['manifest_number', '=', $manifestNumber],
            ])->count();

            if($totalVerifiedUnits == $totalUnits){
                return response()->json([
                    'error'     => false,
                    'response'  => [
                        'unitVerified' => 'The unit has been verified',
                        'checkManifest' => true
                    ]
                ]);             
            }else
            {
                return response()->json([
                    'error'     => false,
                    'response'  => [
                        'unitVerified' => 'The unit has been verified',
                        'checkManifest' => false
                    ]
                ]);             
            } 
        }
    }

    public function verifyManifest(Request $request)
    {
        $manifestNumber = request()->manifestNumber;
        $status = 'RECEIVED';

        $getManifest = DB::table('manifests')->select(DB::raw('manifests.*'))
            ->where('number', $manifestNumber)
            ->first();

        $manifestId = $getManifest->id;

        $manifest = Manifest::find($manifestId);
        $manifest->status = $status;
        $manifest->save();

        return response()->json([
            'error'     => false,
            'response'  => 'The manifest has been confirmed.'
        ]);             
    }
}
