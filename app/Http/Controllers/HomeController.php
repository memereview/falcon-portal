<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->has('user')) {
            $user = session()->get('user');

            if ($user['type'] == 'ADMIN') {
                return redirect('/admin');
            } elseif ($user['type'] == 'CLIENT') {
                return redirect('/client');
            } else {
                return redirect('/login');
            }
        }

        return redirect('/login');
    }
}
