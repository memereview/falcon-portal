<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{

    public function index()
    {
        $user = session()->get('user');

        return view('profile', compact('user'));
    }

    public function update()
    {
        $user = session()->get('user');

        // Validate and store the user creation...
        $validator = Validator::make(request()->all(), [
            'first_name'    => 'required|max:191|alpha_num_space',
            'middle_name'   => 'sometimes|nullable|max:191|alpha_num_space',
            'last_name'     => 'required|max:191|alpha_num_space',
            'email'         => 'required|max:191|unique:users,email,' . $user['id'] . ',id,deleted_at,NULL',
        ], [
            'required'      => 'Required Field',
            'max'           => 'Character limit reached',
            'regex'         => 'Must only contain alphanumeric characters',
            'email'         => 'Must contain a valid email address',
            'unique'        => 'Email already exist'
        ]);

        // set the fields
        $user['first_name'] = request('first_name');
        $user['middle_name'] = request('middle_name');
        $user['last_name'] = request('last_name');
        $user['email'] = request('email');

        if ($validator->fails()) {
            return view('profile', [
                'user'      => $user,
                'errors'    => $validator->errors()
            ]);
        }

        // get the old user data from the database
        $oldUser = User::find($user['id']);

        // old user was not found in the database. must be deleted?
        // anyway, logout
        if (!$oldUser) {
            session()->flush();

            return redirect('/login');
        }

        // now, set the values of the user
        $oldUser->first_name = request('first_name');
        $oldUser->middle_name = request('middle_name');
        $oldUser->last_name = request('last_name');
        $oldUser->email = request('email');

        // update the old user's fields
        $oldUser->save();

        // update field values in the session
        session()->put('user', $oldUser->fieldsForSession());

        return view('profile', [
            'user'      => $user,
            'success'   => 'You have updated your profile information'
        ]);
    }

}