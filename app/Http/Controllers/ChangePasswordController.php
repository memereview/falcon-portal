<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{

    public function index()
    {
        $user = session()->get('user');

        return view('change-password', compact('user'));
    }

    public function store(Request $request)
    {
        $user = session()->get('user');
        $user = User::find($user['id']);

        // Validate the new password
        $this->validate(request(), [
            'current_password'  => 'required|hash_check:' . $user->password,
            'new_password'      => 'required|min:6|current_password_check:' . request()->current_password . '|confirmed'
        ], [
            'required'      => 'Required Field',
            'min'           => 'Minimum should be :min characters',
            'confirmed'     => 'Passwords did not match',
        ]);

        // Update the password
        $user->password = Hash::make(request()->new_password);
        $user->save();

        return view('change-password', [
            'success' => 'You have changed your password'
        ]);
    }

}