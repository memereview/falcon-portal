<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use App\Models\Manifest;
use App\Models\Unit;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AssignUnitsController extends Controller
{
    public function showUnits()
    {

    	$manifest_numbers = array();

    	$manifests = Manifest::where('status', '=', 'RECEIVED')->get();

    	foreach ($manifests as $manifest)
    	{
    		$manifest_numbers[] = $manifest->number;
    	}

//    	$units = Unit::whereIn('manifest_number', $manifest_numbers)->get();
        $units = Unit::has('delivery', '<', 1)
            ->whereIn('manifest_number', $manifest_numbers)
            ->where('status', '!=', 'DELIVERED')
            ->get();

    	$riders = User::where([
    		['type', '=', 'RIDER'],
    		['verified', '=', 1]
    	])->get();

        $deliveries = DB::table('units')
            ->join('deliveries', 'units.id', '=', 'deliveries.unit_id')
            ->join('customers', 'units.customer_id', '=', 'customers.id')
            ->join('customer_addresses', 'units.address_id', '=', 'customer_addresses.id')
            ->select('units.id', 'units.tracking_number', 'units.status', 'deliveries.rider_id', 'deliveries.unit_id', 'customers.name', 'customer_addresses.street', 'customer_addresses.street2', 'customer_addresses.barangay', 'customer_addresses.city', 'customer_addresses.province', 'customer_addresses.postal')
            ->where('units.status', '!=', 'DELIVERED')
            ->get();

    	return view('/admin/assign-units', compact('units', 'riders', 'deliveries'));
    }

    public function assignUnits(Request $request)
    {
        $assignedUnits = request()->set;

        if (!$assignedUnits) {

            Delivery::truncate();

            return response()->json([
                'error'     => false,
                'response'  => "Removed all assigned units"
            ]);
        }

        $assignedIds = array();

        foreach ($assignedUnits as $assignedUnit) {
            $assigned = Delivery::where('unit_id', $assignedUnit[1])->first();

            if (!$assigned) {
                Delivery::create([
                    'rider_id' => $assignedUnit[0],
                    'unit_id' => $assignedUnit[1]
                ]);
            } else {

                $assigned->rider_id = $assignedUnit[0];
                $assigned->unit_id = $assignedUnit[1];
                $assigned->date_assigned = DB::raw('NOW()');

                $assigned->save();
            }

            //Search for matching delivery
            $getAssignedId = DB::table('deliveries')
                            ->where('rider_id', '=', $assignedUnit[0])
                            ->where('unit_id', '=', $assignedUnit[1])
                            ->select('deliveries.id')
                            ->get();

            array_push($assignedIds, $getAssignedId[0]->id);
        }

        $remove = Delivery::whereNotIn('id', $assignedIds)->delete();

        return response()->json([
            'error'     => false,
            'response'  => "The units have been assigned."
        ]);
    }

    public function autoAssign()
    {
        $riderIds = array();
        $set = array();

        $riders = User::where([
            ['type', '=', 'RIDER'],
            ['verified', '=', 1]
        ])->get();

        $riderCount = count($riders);
        $assignCounter = 0;

        foreach ($riders as $rider) {
           array_push($riderIds, $rider->id);
        }


        $manifest_numbers = array();

        $manifests = Manifest::where('status', '=', 'RECEIVED')->get();

        foreach ($manifests as $manifest)
        {
            $manifest_numbers[] = $manifest->number;
        }

        $units = Unit::has('delivery', '<', 1)
            ->whereIn('manifest_number', $manifest_numbers)
            ->where('status', '!=', 'DELIVERED')
            ->get();

        foreach ($units as $unit) {
            if($assignCounter < $riderCount) {
                Delivery::create([
                    'rider_id' => $riderIds[$assignCounter],
                    'unit_id' => $unit->id
                ]);
                $assignCounter++;
            }
            else {
                $assignCounter = 0;
                Delivery::create([
                    'rider_id' => $riderIds[$assignCounter],
                    'unit_id' => $unit->id
                ]);
                $assignCounter++;
            }
        }

        return response()->json([
            'error'     => false,
            'response'  => "The units have been automatically assigned."
        ]);
    }

    public function clearAssign()
    {
        $deleteIds = array();

        //Search for matching delivery
        $assignedIds = DB::table('deliveries')
                        ->join('units', 'deliveries.unit_id', '=', 'units.id')
                        ->select('units.status', 'deliveries.unit_id', 'deliveries.rider_id', 'deliveries.id')
                        ->where('units.status', '!=', 'IN TRANSIT')
                        ->get();

        foreach ($assignedIds as $assignedId) {
            array_push($deleteIds, $assignedId->id);
        }

        $remove = Delivery::whereIn('id', $deleteIds)->delete();

        return response()->json([
            'error'     => false,
            'response'  => 'Removed all assigned units'
        ]);
    }
}
