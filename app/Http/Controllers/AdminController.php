<?php
/**
 * Created by PhpStorm.
 * User: Sherwin
 * Date: 2/25/2018
 * Time: 3:35 PM
 */

namespace App\Http\Controllers;


class AdminController extends Controller
{
    public function index()
    {
        return redirect('/admin/users');
    }
}