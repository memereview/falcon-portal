<?php
/**
 * Created by PhpStorm.
 * User: Sherwin
 * Date: 2/25/2018
 * Time: 12:45 PM
 */

namespace App\Http\Controllers;


use App\Models\User;
use App\Models\UserLogs;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;

class SessionController extends Controller
{
    public function index()
    {
        if (session()->has('user')) {
            $user = session()->get('user');

            if ($user['type'] == 'ADMIN') {
                return redirect('/admin');
            } elseif ($user['type'] == 'CLIENT') {
                return redirect('/client');
            }
        }

        return view('login', [
            'success_message' => session('success_message') ?? null
        ]);
    }

    public function login()
    {
        $this->validate(request(), [
            'email'     => 'required|email',
            'password'  => 'required'
        ], [
            'required'  => 'Required field',
            'email'     => 'Must contain a valid email address'
        ]);

        $user = User::where('email', request('email'))->first();

        if (!$user || !in_array($user->type, User::VALID_WEB_USERS)) {
            return view('login', [
                'error_message' => 'Invalid Credentials'
            ]);
        }

        $password = request('password');
        $hash = $user->password;

        if (!Hash::check($password, $hash)) {
            return view('login', [
                'error_message' => 'Invalid Credentials'
            ]);
        }

        if (Hash::needsRehash($hash)) {
            $newHash = Hash::make($password);

            $user->password = $newHash;

            $user->save();
        }

        if (!$user->verified) {
            return view('login', [
                'error_message' => 'Please verify your account first to access Falcon'
            ]);
        }

        session(['user' => $user->fieldsForSession()]);

        UserLogs::create([
           'user_id'      => $user->id,
           'ip_address'   => Request::ip(),
           'user_type'    => $user->type
        ]);

        $user_id = $user->id;

        if ($user->type == User::TYPE_ADMIN) {
            return redirect('/admin');
        } elseif ($user->type == User::TYPE_CLIENT) {
            return view('client.upload-manifest')->with('user_id', $user_id);
        }
    }

    public function logout()
    {
        session()->flush();

        return redirect('/login');
    }

}