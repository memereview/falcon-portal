<?php
/**
 * Created by PhpStorm.
 * User: Sherwin
 * Date: 2/25/2018
 * Time: 4:54 PM
 */

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\Manifest;
use App\Models\Unit;
use App\Models\User;
use App\Mail\EmailUploaded;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Validator;

class ClientController extends Controller
{
    public function index()
    {
        return redirect('/client/upload-manifest');
    }

    public function showDetails($id)
    {
        $company_name = '(
                SELECT company_name 
                FROM clients 
                WHERE user_id = ' . $id .'
                ORDER BY date_created DESC
                LIMIT 1
            ) as company_name';

        $latestLoginDate = '(
                SELECT date_login 
                FROM user_logs
                WHERE user_id = ' . $id .'
                ORDER BY date_login DESC
                LIMIT 1
            ) as date_login';

        $user = DB::table('users')->select(DB::raw('users.*, ' . $latestLoginDate . "," . $company_name))
            ->where('id', $id)
            ->first();

        $clients = Client::where('user_id', $user->id)->first();

        return view('client/details', compact('user', 'clients'));
    }

    public function changePassword($id)
    {
        $client = User::find($id);

        return view('client/change-password', compact('client'));
    }

    public function updatePassword(Request $request, $id)
    {
        $user = User::find($id);
        if(!Hash::check(request()->current_password, $user->password)){
            //password matches
            return redirect()->back()->with("error","Your current password does not match with your password provided. Please try again.");
        }

        if(strcmp(request()->current_password, request()->new_password) == 0){
            //current password and new password are the same
            return redirect()->back()->with("error", "New password cannot be the same as your current password. Please try again.");
        }
        
        // Validate the new password
        $this->validate(request(), [
            'new_password'    => 'required|min:6|confirmed'
        ], [
            'required'      => 'Required Field',
            'min'           => 'Minimum should be 6 characters',
            'confirmed'     => 'Passwords did not match',
        ]);

        // Update the password
        $user->password = bcrypt(request()->new_password);    
        $user->save();

        return redirect()->action('ClientController@showDetails', ['id' => $id]);
    }

    public function showUploader(Request $request)
    {
        $user = session()->get('user');
        $user_id = $user['id'];
        return view('/client/upload-manifest', compact('user_id'));
    }

    public function fileUploader(Request $request)
    {
        $user = session()->get('user');
        $user_id = $user['id'];

        $file = $request->file('manifest');

        if ($request->hasFile('manifest')) {
            $filename = $file->getClientOriginalName();

            $validateManifest = Validator::make([
                'manifest'  => $file,
                'file_name' => $filename
            ], [
                'manifest'  => 'required|max:255|mimes:csv,txt',
                'file_name' => ' unique:manifests,file_name'
            ], [
                'required'  => 'File required',
                'mimes'     => 'Must upload a CSV file',
                'unique'    => 'This File has already been uploaded'
            ]);
        } else {
            $validateManifest = Validator::make([
                'manifest'  => $file
            ], [
                'manifest'  => 'required|max:255|mimes:csv,txt'
            ], [
                'required'  => 'File required',
                'mimes'     => 'Must upload a CSV file'
            ]);
        }

        if ($validateManifest->fails()) {
            return view('/client/upload-manifest', compact('user_id'))
                ->withErrors($validateManifest);
        }

        if ($request->hasFile('manifest')) {
            $userId = session()->get('user.id');
            $client = Client::where('user_id', '=', $userId)->first();
            $clientId = $client->id;
//            $filename = $request->manifest->getClientOriginalName();
            $result = Manifest::count();
            $manifestNum = '10001' + $result;
            $hasError = false;

            // generate csv error report file
            $errorFileName = storage_path('app/public/error_' . $filename);
            $errorFile = fopen($errorFileName, 'w');
            $columns = array('Error Message', 'Line');

            fputcsv($errorFile, $columns);

            $storedFile = $request->manifest->storeAs('public', $filename);
            $uploadedFile = storage_path('app/' . $storedFile);
            $handle = fopen($uploadedFile, "r");
            $line = 0;

            fgets($handle);  // read one line for nothing (skip header)

            DB::beginTransaction();

            Manifest::create([
                'client_id'  => $clientId,
                'file_name'  => $filename,
                'number'     => $manifestNum,
                'status'     => 'NEW'
            ]);

            while (($data = fgetcsv($handle, 0, ',')) !== false) {

                $line++;

                if(count($data) == 18) {

                    $trackingNum    = $data[0];
                    $customerName   = preg_replace(
                        '/[\s]+/',
                        ' ',
                        $data[1]." ".$data[2]." ".$data[3]);
                    $phoneNumber    = $data[4];
                    $email          = $data[5];
                    $street1        = $data[6];
                    $street2        = $data[7];
                    $barangay       = $data[8];
                    $city           = $data[9];
                    $province       = $data[10];
                    $postalCode     = $data[11];
                    $length         = $data[12];
                    $width          = $data[13];
                    $height         = $data[14];
                    $weight         = $data[15];
                    $collection     = $data[16];
                    $remarks        = $data[17];

                    //Manifest Content validation
                    $validator = Validator::make([
                            'tracking_number'   => $trackingNum,
                            'first_name'        => str_replace
                                (' ', '', $data[1]),
                            'middle_name'       => str_replace
                                (' ', '', $data[2]),
                            'last_name'         => str_replace
                                (' ', '', $data[3]),
                            'phone_number'      => $phoneNumber,
                            'email'             => $email,
                            'street_1'          => $street1,
                            'street_2'          => $street2,
                            'barangay'          => $barangay,
                            'city'              => $city,
                            'province'          => $province,
                            'length'            => $length,
                            'width'             => $width,
                            'height'            => $height,
                            'weight'            => $weight,
                            'postal_code'       => $postalCode,
                            'collection'        => $collection,
                        ], [
                            'tracking_number'   => 'required|alpha_num|
                                unique:units,tracking_number,NULL,id,client_id,' . $clientId,
                            'first_name'        => 'required|alpha',
                            'middle_name'       => 'alpha',
                            'last_name'         => 'required|alpha',
                            'phone_number'      => 'required|numeric',
                            'email'             => 'required|email',
                            'street_1'          => 'required',
                            'city'              => 'required',
                            'province'          => 'required',
                            'length'            => 'required|numeric',
                            'width'             => 'required|numeric',
                            'height'            => 'required|numeric',
                            'weight'            => 'required|numeric',
                            'postal_code'       => 'required|numeric|digits:4',
                            'collection'        => 'required|numeric',
                        ], [
                            'unique'        => ':attribute already exists',
                            'required'      => ':attribute is required',
                            'numeric'       => ':attribute must be numeric',
                            'alpha'         => ':attribute must be alphabetic ' .
                                'characters',
                            'email'         => ':attribute must be email',
                            'max'           => ':attribute exceeds limit of :max',
                            'digits'        => ':attribute must always be 4 digits'
                        ]);

                    if ($validator->fails()) {
                        $hasError = true;
                        $errorMessage = "";

                        foreach($validator->errors()->all(':message') as $error) {

                            if ($errorMessage) {
                                $errorMessage .= ', ';
                            }

                            $errorMessage .= $error;

                        }

                        fputcsv($errorFile, array($errorMessage, 'Row ' . $line));

                        continue;
                    }

                    //insert customers
                    $customer = Customer::where('name', '=', $customerName)->first();

                    if (!$customer) {
                        $customer = Customer::create([
                            'name' => $customerName,
                        ]);
                    }

                    $customerAddress = CustomerAddress::where([
                        ['customer_id', '=', $customer->id],
                        ['phone',       '=', $phoneNumber],
                        ['email',       '=', $email],
                        ['street',      '=', $street1],
                        ['street2',     '=', $street2],
                        ['barangay',    '=', $barangay],
                        ['city',        '=', $city],
                        ['province',    '=', $province],
                        ['postal',      '=', $postalCode],
                    ])->first();

                    if (!$customerAddress) {
                        //Insert customer address
                        $customerAddress = CustomerAddress::create([
                            'customer_id'   => $customer->id,
                            'phone'         => $phoneNumber,
                            'email'         => $email,
                            'street'        => $street1,
                            'street2'       => $street2,
                            'barangay'      => $barangay,
                            'city'          => $city,
                            'province'      => $province,
                            'postal'        => $postalCode,
                            'country'       => 'PH'
                        ]);
                    }

                    //insert Unit information
                    Unit::create([
                        'customer_id'       => $customer->id,
                        'address_id'        => $customerAddress->id,
                        'manifest_number'   => $manifestNum,
                        'tracking_number'   => $trackingNum,
                        'length'            => $length,
                        'width'             => $width,
                        'height'            => $height,
                        'weight'            => $weight,
                        'collection'        => $collection,
                        'status'            => 'NEW',
                        'remarks'           => $remarks,
                        'client_id'         => $clientId
                    ]);
                }else {
                    //invalid column count error file
                    $hasError = True;
                    $errorMessage = 'Invalid column count';
                    fputcsv($errorFile, array($errorMessage, 'Row' . $line));
//                    continue;
                }
            }


            fclose($handle);
            fclose($errorFile);
            unlink($uploadedFile);

            if ($hasError) {
                DB::rollBack();

                return view('/client/upload-manifest', compact('user_id'))
                    ->with('filename', $filename)
                    ->withErrors($errorMessage);
            }

            unlink($errorFileName);
            DB::commit();

            // Send Email to all Admin on successful upload
            $admins = User::where('type', 'admin')->get();

            foreach ($admins as $admin) {

                $emailUploaded = new EmailUploaded($admin, $filename);

                if (config('app.env') === 'production') {
                    Mail::to($admin->email)->queue($emailUploaded);
                } else {
                    Mail::to($admin->email)->send($emailUploaded);
                }
            }

            return redirect('/manifests/new/units/'.$manifestNum);
        }
    }

    function downloadErrorFile($file) {
        return response()->download(storage_path('app/public/error_' . $file))
            ->deleteFileAfterSend(true);
    }

}