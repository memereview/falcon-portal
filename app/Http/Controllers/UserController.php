<?php

namespace App\Http\Controllers;

use App\Mail\EmailVerify;
use App\Models\Client;
use App\Models\User;
use App\Models\UserVerification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Ramsey\Uuid\Uuid;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = session()->get('user');
        $userId = $user['id'];

        $latestLoginDate = '(
                SELECT date_login 
                FROM user_logs
                WHERE user_id = users.id
                ORDER BY date_login DESC
                LIMIT 1
            ) as date_login';
        $users = DB::table('users')->select(DB::raw('users.*, ' . $latestLoginDate))
            ->where('deleted_at', '=', null)
            ->paginate(5);

        // $users = User::select(User::raw("SELECT 'users.*', 'user_logs.date_login' FROM users INNER JOIN user_logs ON 'users.id' = 'user_logs.user_id'"));


        return view('admin/user/list', compact('users', 'userId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/user/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (request()->type == "CLIENT") {
            // Validate and store the user creation...
            $this->validate(request(), [
                'first_name'    => 'required|max:191|alpha_num_space',
                'middle_name'   => 'sometimes|nullable|max:191|alpha_num_space',
                'last_name'     => 'required|max:191|alpha_num_space',
                'email'         => 'required|email|max:191|unique:users,email,NULL,id,deleted_at,NULL',
                'company_name'  => 'required|max:191',
                'street_1'      => 'required|max:191',
                'street_2'      => 'sometimes|nullable|max:191',
                'barangay'      => 'sometimes|nullable|max:191',
                'city'          => 'required|max:191|alpha_num_space',
                'province'      => 'required|max:191|alpha_num_space',
                'postal_code'   => 'required|numeric|digits:4'

            ], [
                'required'      => 'Required Field',
                'max'           => 'Character limit reached',
                'regex'         => 'Must only contain alphanumeric characters',
                'email'         => 'Must contain a valid email address',
                'unique'        => 'Email already exist'
            ]);
        }else{
             // Validate and store the user creation...
            $this->validate(request(), [

                'first_name'    => 'required|max:191|alpha_num_space',
                'middle_name'   => 'sometimes|nullable|max:191|alpha_num_space',
                'last_name'     => 'required|max:191|alpha_num_space',
                'email'         => 'required|email|max:191|unique:users,email,NULL,id,deleted_at,NULL',
            ], [
                'required'      => 'Required Field',
                'max'           => 'Character limit reached',
                'regex'         => 'Must only contain alphanumeric characters',
                'email'         => 'Must contain a valid email address',
                'unique'        => 'Email already exist'
            ]);
        }
       

        // The user validation is valid...
        $user = User::create(request()->all());

        if (request()->type == "CLIENT") {
            Client::create([
                'user_id'       => $user->id,
                'company_name'  => request('company_name'),
                'street'        => request('street_1'),
                'street2'       => request('street_2'),
                'barangay'      => request('barangay'),
                'city'          => request('city'),
                'province'      => request('province'),
                'postal'        => request('postal_code'),
                'country'       => 'PH'
            ]);
        }
        

        // create user verification
        $userVerification = UserVerification::create([
            'user_id'   => $user->id,
            'token'     => Uuid::uuid1()->toString()
        ]);

        // send an email to the user for verification
        $emailVerify = new EmailVerify($user, $userVerification);

        if (config('app.env') === 'production') {
            Mail::to(request('email'))->queue($emailVerify);
        } else {
            Mail::to(request('email'))->send($emailVerify);
        }

        return redirect('/admin/users/' . $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = session()->get('user');
        $userId = $user['id'];

        $companyName = '(
                SELECT company_name 
                FROM clients 
                WHERE user_id = ' . $id .'
                ORDER BY date_created DESC
                LIMIT 1
            ) as company_name';

        $latestLoginDate = '(
                SELECT date_login 
                FROM user_logs
                WHERE user_id = ' . $id .'
                ORDER BY date_login DESC
                LIMIT 1
            ) as date_login';

        $user = DB::table('users')->select(DB::raw('users.*, ' . $latestLoginDate . "," .
            $companyName))
            ->where('id', $id)
            ->first();

        if (is_null($user->date_login)) {
            $user->date_login = "This user has not yet logged in for the first time.";
        }

        $clients = Client::where('user_id', $user->id)->first();

        return view('admin/user/show', compact('user', 'userId', 'clients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $client = Client::where('user_id', $user->id)->first();
        return view('admin/user/edit', compact('user', 'client'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $trashedUsers =  User::onlyTrashed()->get();
        $ignoreId = array();
        foreach ($trashedUsers as $trashedUser) {
            array_push($ignoreId, $trashedUser->id);
        }

        if (request()->type == "CLIENT") {
            // Validate and store the user creation...
            $this->validate(request(), [
                'first_name'    => 'required|max:191|alpha_num_space',
                'middle_name'   => 'sometimes|nullable|max:191|alpha_num_space',
                'last_name'     => 'required|max:191|alpha_num_space',
                'email'         => 'required|max:191|unique:users,email,' . $user->id . ',id,deleted_at,NULL',
                'company_name'  => 'required|max:191',
                'street_1'      => 'required|max:191',
                'street_2'      => 'sometimes|nullable|max:191',
                'barangay'      => 'sometimes|nullable|max:191',
                'city'          => 'required|max:191|alpha_num_space',
                'province'      => 'required|max:191|alpha_num_space',
                'postal_code'   => 'required|numeric|digits:4'
            ], [
                'required'      => 'Required Field',
                'max'           => 'Character limit reached',
                'regex'         => 'Must only contain alphanumeric characters',
                'email'         => 'Must contain a valid email address',
                'unique'        => 'Email already exist'
            ]);

        } else {
            // Validate and store the user creation...
            $this->validate(request(), [
                'first_name'    => 'required|max:191|alpha_num_space',
                'middle_name'   => 'sometimes|nullable|max:191|alpha_num_space',
                'last_name'     => 'required|max:191|alpha_num_space',
                'email'         => 'required|max:191|unique:users,email,' . $user->id . ',id,deleted_at,NULL',
            ], [
                'required'      => 'Required Field',
                'max'           => 'Character limit reached',
                'regex'         => 'Must only contain alphanumeric characters',
                'email'         => 'Must contain a valid email address',
                'unique'        => 'Email already exist'
            ]);
        }

        // Update Admin
        $user->first_name = $request->first_name;
        $user->middle_name = $request->middle_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->type = $request->type;

        $user->save();

        // Update Client
        if (request()->type == "CLIENT") {
            $client = Client::where('user_id', $user->id)->first();

//            $client->user_id = $user->id;
            $client->company_name = $request->company_name;
            $client->street = $request->street_1;
            $client->street2 = $request->street_2;
            $client->barangay = $request->barangay;
            $client->city = $request->city;
            $client->province = $request->province;
            $client->postal = $request->postal_code;

            $client->save();
        }

        return redirect('/admin/users/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //deleting the user
        $user = User::find($id);
        $user->delete();

        return response()->json([
            'code'      => 200,
            'response'  => 'Ok'
        ]);
    }

    /**
     * Show the verification page
     *
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showVerification($token) {
        $result = $this->validateToken($token);

        if ($result) {
            return $result;
        }

        return view('public/user-verify', [
            'token' => $token
        ]);
    }

    /**
     * Verify the user using the token and the password
     *
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|
     * \Illuminate\Routing\Redirector|\Illuminate\View\View|null
     */
    public function verify($token) {
        $result = $this->validateToken($token);

        if ($result) {
            return $result;
        }

        $this->validate(request(), [
            'password'    => 'required|min:6|confirmed'
        ], [
            'required'      => 'Required Field',
            'min'           => 'Minimum should be 6 characters',
            'confirmed'     => 'Passwords did not match',
        ]);

        $userVerification = UserVerification::where('token', $token)->first();
        $user = User::find($userVerification->user_id);

        // delete verification
        $userVerification->delete();

        // update user's verification status and password
        $user->verified = 1;
        $user->password = Hash::make(request('password'));
        $user->save();

        return redirect('/login');
    }

    /**
     * Check if the token and the user are valid
     *
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|null
     */
    private function validateToken($token) {
        $userVerification = UserVerification::where('token', $token)->first();

        if (!$userVerification) {
            return view('/login');
        }

        $hasUser = User::where('id', $userVerification->user_id)
                        ->whereNull('deleted_at')
                        ->count();

        // user not found, delete verification record
        if (!$hasUser) {
            $userVerification->delete();

            return view('/login');
        }

        return null;
    }

}
