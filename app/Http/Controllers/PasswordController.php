<?php
/**
 * Created by PhpStorm.
 * User: Sherwin
 * Date: 2/25/2018
 * Time: 3:35 PM
 */

namespace App\Http\Controllers;

use App\Mail\EmailReset;
use App\Models\ResetPassword;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Ramsey\Uuid\Uuid;

class PasswordController extends Controller
{

    public function sendResetToken()
    {
        $user = User::where('email', request('email'))->first();

        if (!$user || !in_array($user->type, User::VALID_WEB_USERS)) {
            return view('forgot-password', [
                'error_message' => 'The Email Address does not exist'
            ]);
        }

        //create token
        $resetPassword = ResetPassword::create([
            'email' => request('email'),
            'token' => Uuid::uuid1()->toString()
        ]);

        ResetPassword::where('email', request('email'))->first();

        //send email
        $emailReset = new EmailReset($user, $resetPassword);

        if (config('app.env') == 'production') {
            Mail::to(request('email'))->queue($emailReset);
        } else {
            Mail::to(request('email'))->send($emailReset);
        }

        return view('forgot-password', [
            'success_message' => 'An Email was sent with the instructions on how to reset your ' .
                'password'
        ]);
    }

    public function showResetForm($token)
    {
        $tokenData = ResetPassword::where('token', $token)->first();

        //check if token exists
        if (!$tokenData) {
            return redirect('/login');
        }

        return view('reset-password',  ['token' => $token]);
    }

    public function resetPassword($token)
    {
        $password = request('password');

        $this->validate(request(), [
            'password'    => 'required|min:6|confirmed'
        ], [
            'required'      => 'Required Field',
            'min'           => 'Minimum should be 6 characters',
            'confirmed'     => 'Passwords did not match',
        ]);

        $tokenData = ResetPassword::where('token', $token)->first();

        $user = User::where('email', $tokenData->email)->first();

        if (!$user) {
            return redirect('/login');
        }

        //update new password
        $user->password = Hash::make($password);
        $user->save();

        //Delete token after update
        ResetPassword::where('email', $user->email)->delete();

        return redirect('/login')->with('success_message', 'Your password was successfully ' .
            'changed. You may now login');
    }

}