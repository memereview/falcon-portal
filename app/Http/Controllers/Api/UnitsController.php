<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Mail\EmailClientDelivered;
use App\Mail\EmailAdminDelivered;
use App\Mail\EmailDelivered;
use App\Mail\EmailTransit;
use App\Models\User;
use App\Models\Client;
use App\Models\CustomerAddress;
use App\Models\Manifest;
use App\Models\Returned;
use App\Models\Unit;
use App\Models\DeliveryConfirmation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Ramsey\Uuid\Uuid;


class UnitsController extends Controller
{

    /**
     * Gets the assigned unit to the user id using the token provided by the middleware {@link CheckRider}
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function assigned()
    {
        // user from the middleware
        $user = request()->user;

        // get all the units using the user's id
        $units = DB::table('units')
            ->join('deliveries', 'deliveries.unit_id', '=', 'units.id')
            ->join('customers', 'customers.id', '=', 'units.customer_id')
            ->where('deliveries.rider_id', $user->id)
            ->select(
                'units.id',
                'units.manifest_number as manifest_no',
                'units.tracking_number as tracking',
                'customers.name as customer',
                'units.collection',
                'units.length',
                'units.width',
                'units.height',
                'units.weight',
                'deliveries.date_assigned as received',
                'units.status',
                'units.address_id',
                'units.customer_id'
            )
            ->get();

        foreach ($units as $unit) {
            $address = CustomerAddress::where('id', $unit->address_id)
                ->where('customer_id', $unit->customer_id)
                ->first();

            // attach the address
            $unit->address = [
                'phone'     => $address->phone,
                'street'    => $address->street,
                'street2'   => $address->street2,
                'barangay'  => $address->barangay,
                'city'      => $address->city,
                'province'  => $address->province,
                'postal'    => $address->postal,
                'country'   => $address->country
            ];

            // set the returned records
            $unit->returned = Returned::where('unit_id', $unit->id)->get();

            // set the default confirmed setting to 'YES'
            $unit->confirmed = 'YES';
        }

        return response()->json([
            'code'      => 200,
            'response'  => $units
        ]);
    }

    /**
     * Sets the unit to delivering using the user id using the token provided by the middleware {@link CheckRider}
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delivering()
    {
        // user from the middleware
        $user = request()->user;
        // tracking number
        $tracking = request('tracking');

        // if the tracking number is empty, return error message
        if (!isset($tracking) || empty($tracking)) {
            return response()->json([
                'code'      => 400,
                'response'  => 'Tracking number is required'
            ]);
        }

        // update the unit using the user id and the tracking number
        DB::table('units')
            ->join('deliveries', 'deliveries.unit_id', '=', 'units.id')
            ->where('deliveries.rider_id', $user->id)
            ->where('units.tracking_number', $tracking)
            ->update(['status' => Unit::STATUS_IN_TRANSIT]);

        // Send Email to Customer
        $unit = Unit::has('address')->where('tracking_number', '=', $tracking)->first();
        $emailTransit = new EmailTransit($unit);

        if (config('app.env') === 'production') {
            Mail::to($unit->address->email)->queue($emailTransit);
        } else {
            Mail::to($unit->address->email)->send($emailTransit);
        }

        return response()->json([
            'code'      => 200,
            'response'  => 'Ok'
        ]);
    }

    /**
     * Sets the unit to delivered using the user id using the token provided by the middleware {@link CheckRider}
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delivered()
    {
        // user from the middleware
        $user = request()->user;
        // tracking number
        $tracking = request('tracking');

        // if the tracking number is empty, return error message
        if (!isset($tracking) || empty($tracking)) {
            return response()->json([
                'code'      => 400,
                'response'  => 'Tracking number is required'
            ]);
        }

        // update the unit using the user id and the tracking number
        DB::table('units')
            ->join('deliveries', 'deliveries.unit_id', '=', 'units.id')
            ->where('deliveries.rider_id', $user->id)
            ->where('units.tracking_number', $tracking)
            ->update(['status' => Unit::STATUS_CONFIRMATION]);

        // Get customer
        $unit = Unit::has('address')->where('tracking_number', '=', $tracking)->first();

        //create confirmation token
        $confirmation = DeliveryConfirmation::create([
            'unit_id' => $unit->id,
            'token' => Uuid::uuid1()->toString()
        ]);

        // Send email to customer
        $emailDelivered = new EmailDelivered($unit, $confirmation);

        if (config('app.env') === 'production') {
            Mail::to($unit->address->email)->queue($emailDelivered);
        } else {
            Mail::to($unit->address->email)->send($emailDelivered);
        }

//        //Get manifest number of tracking number
//        $manifestNumber = Unit::where('tracking_number', $tracking)->first()->manifest_number;
//
//        //Count total units of manifest
//        $totalUnits = Unit::where('manifest_number', $manifestNumber)->count();
//
//        //Get all units with delivered status and count
//        $deliveredUnits = DB::table('units')
//            ->where('units.status', 'DELIVERED')
//            ->where('units.manifest_number', $manifestNumber)
//            ->get();
//        $totalDelivered = count($deliveredUnits);
//
//        if ($totalUnits == $totalDelivered) {
//            // update the manifest status to completed
//            DB::table('manifests')
//                ->where('manifests.number', $manifestNumber)
//                ->update(['status' => Manifest::STATUS_COMPLETED]);
//
//            // send email to the client that uploaded the manifest
//            $manifest = Manifest::has('client')->where('number', $manifestNumber)->first();
//            $client = Client::has('user')->where('id', $manifest->client->id)->first();
//
//            $emailClientDelivered = new EmailClientDelivered($client, $manifest);
//
//            if (config('app.env') === 'production') {
//                Mail::to($client->user->email)->queue($emailClientDelivered);
//            } else {
//                Mail::to($client->user->email)->send($emailClientDelivered);
//            }
//
//            $admins = User::where('type', 'admin')->get();
//
//            foreach ($admins as $admin) {
//
//            $emailAdminDelivered = new EmailAdminDelivered($admin, $manifest);
//
//                if (config('app.env') === 'production') {
//                    Mail::to($admin->email)->queue($emailAdminDelivered);
//                } else {
//                    Mail::to($admin->email)->send($emailAdminDelivered);
//                }
//            }
//        }

        return response()->json([
            'code'      => 200,
            'response'  => 'Ok'
        ]);
    }

    /**
     * Sets the unit to cancelled using the user id using the token provided by the middleware {@link CheckRider}
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function returned()
    {
        // user from the middleware
        $user = request()->user;
        // tracking number
        $tracking = request('tracking');
        // issue number
        $issue = request('issue');

        // check the issue number set
        if ($issue - 1 < 0 || ($issue - 1) > sizeof(Unit::RETURNED_STATUS_ARRAY) - 1) {
            return response()->json([
                'code'      => 400,
                'response'  => 'Invalid issue'
            ]);
        }

        // issue string
        $issueReason = Unit::RETURNED_STATUS_ARRAY[$issue - 1];

        // if the tracking number is empty, return error message
        if (!isset($tracking) || empty($tracking)) {
            return response()->json([
                'code'      => 400,
                'response'  => 'Tracking number is required'
            ]);
        }

        // update the unit using the user id and the tracking number
        DB::table('units')
            ->join('deliveries', 'deliveries.unit_id', '=', 'units.id')
            ->where('deliveries.rider_id', $user->id)
            ->where('units.tracking_number', $tracking)
            ->update(['status' => Unit::STATUS_CANCELLED]);

        Returned::create([
            'issue'     => $issue,
            'remarks'   => $issueReason,
            'unit_id'   => Unit::where('tracking_number', $tracking)->first()->id
        ]);

        return response()->json([
            'code'      => 200,
            'response'  => 'Ok'
        ]);
    }

}