<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ChangePasswordController extends Controller
{

    public function update()
    {
        $user = request()->user;

        // Validate the new password
        $validator = Validator::make(request()->all(), [
            'current_password'  => 'required|hash_check:' . $user->password,
            'new_password'      => 'required|min:6|current_password_check:' . request()->current_password . '|confirmed'
        ], [
            'required'      => 'Required Field',
            'min'           => 'Minimum should be :min characters',
            'confirmed'     => 'Passwords did not match',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            return response()->json([
                'code'      => '400',
                'response'  => [
                    'current_password'          => $errors->first('current_password'),
                    'new_password'              => $errors->first('new_password'),
                    'new_password_confirmation' => $errors->first('new_password_confirmation')
                ]
            ]);
        }

        // Update the password
        $user->password = Hash::make(request()->new_password);
        $user->save();

        return response()->json([
            'code'      => 200,
            'response'  => 'Ok'
        ]);
    }

}