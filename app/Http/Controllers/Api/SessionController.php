<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Session;
use App\Models\User;
use App\Models\UserLogs;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;

class SessionController extends Controller
{

    public function login()
    {
        $email = request('email');
        $password = request('password');

        if (!isset($email) || empty($email) || !isset($password) || empty($password)) {
            return response()->json([
                'code'      => 401,
                'response'  => 'Invalid Credentials'
            ]);
        }

        $user = User::where('email', request('email'))->first();

        if (!$user || !in_array($user->type, User::VALID_API_USERS)) {
            return response()->json([
                'code'      => 401,
                'response'  => 'Invalid Credentials'
            ]);
        }

        $hash = $user->password;

        if (!Hash::check($password, $hash)) {
            return response()->json([
                'code'      => 401,
                'response'  => 'Invalid Credentials'
            ]);
        }

        if (Hash::needsRehash($hash)) {
            $newHash = Hash::make($password);

            $user->password = $newHash;

            $user->save();
        }

        if (!$user->verified) {
            return response()->json([
                'code'      => 401,
                'response'  => 'Please verify your account first to access Falcon'
            ]);
        }

        session(['user' => $user->fieldsForSession()]);

        // create user logs
        UserLogs::create([
            'user_id'      => $user->id,
            'ip_address'   => Request::ip(),
            'user_type'    => $user->type
        ]);

        // delete other session of the user first
        Session::where('user_id', $user->id)->delete();

        // create session data
        Session::create([
            'user_id'   => $user->id,
            'token'     => session()->getId()
        ]);

        return response()->json([
            'code'      => 200,
            'response'  => [
                'id'        => $user->id,
                'token'     => session()->getId(),
                'name'      => $user->first_name . ' ' . $user->last_name,
                'email'     => $user->email
            ]
        ]);
    }

    public function logout()
    {
        // get the authentication token
        $token = request()->header('x-auth-token');

        // delete the session of the rider
        Session::where('token', $token)->delete();

        return response()->json([
            'code'      => 200,
            'response'  => 'Ok'
        ]);
    }

}