<?php

namespace App\Http\Middleware;

use Closure;

class CheckClient
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('user')) {

            $user = session()->get('user');

            if ($user['type'] == 'CLIENT') {
                return $next($request);
            } else {
                return redirect('/403');
            }
        } else {
            return redirect('/login');
        }
    }
}
