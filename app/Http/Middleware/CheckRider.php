<?php


namespace App\Http\Middleware;

use App\Models\Session;
use App\Models\User;
use Closure;

class CheckRider
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // get the authentication token
        $token = $request->header('x-auth-token');

        // if there is no token found
        if (!isset($token)) {
            return response()->json([
                'code'      => 401,
                'response'  => 'You are unauthorized'
            ]);
        }

        // get the rider's session using the token provided
        $session = Session::where('token', $token)->first();

        // if the rider doesn't have a session
        if (!$session) {
            return response()->json([
                'code'      => 401,
                'response'  => 'You are unauthorized'
            ]);
        }

        // get the session's user
        $user = $session->user;

        // check if the user type is a RIDER
        if (!$user || $user->type !== User::TYPE_RIDER) {
            return response()->json([
                'code'      => 403,
                'response'  => 'You cannot access this url'
            ]);
        }

        // set the request's user
        $request->user = $user;

        return $next($request);
    }
}