<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use App\Models\Unit;

class Manifest extends Model
{
    use Notifiable;

    public $timestamps = false;

    public const STATUS_COMPLETED   = "COMPLETED";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'file_name', 'number', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function client()
    {
        return $this->hasOne('App\Models\Client', 'id', 'client_id');
    }

    public function units()
    {
        return $this
            ->hasMany('App\Models\Unit', 'manifest_number', 'number');
    }

}