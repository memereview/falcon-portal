<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rider_id', 'unit_id'
    ];

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'id', 'unit_id');
    }

}
