<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Session extends Model
{
    use Notifiable;

    public $timestamps = false;

    /**
     * Fields that are assignable
     *
     * @var array
     */
    protected $fillable = ['token', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

}