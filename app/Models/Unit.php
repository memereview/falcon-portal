<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Unit extends Model
{
    use Notifiable;

    public const STATUS_VERIFIED   = "VERIFIED";
    public const STATUS_IN_TRANSIT = "IN TRANSIT";
    public const STATUS_DELIVERED  = "DELIVERED";
    public const STATUS_CANCELLED  = "CANCELLED";
    public const STATUS_CONFIRMATION = "FOR CONFIRMATION";
    public const STATUS_FAILED = "FAILED";

    public const RETURNED_STATUS_ARRAY = [
        "Change of mind",
        "Damaged item",
        "Late delivery",
        "No receiver",
        "Wrong item"
    ];

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'address_id', 'manifest_number', 'tracking_number',
        'length', 'width', 'height', 'weight', 'collection', 'status', 'remarks',
        'client_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function customer()
    {
        return $this
            ->hasOne('App\Models\Customer', 'id', 'customer_id');
    }

    public function address()
    {
        return $this
            ->hasOne('App\Models\CustomerAddress', 'id', 'address_id');
    }

    public function delivery()
    {
        return $this
            ->hasOne('App\Models\Delivery', 'unit_id', 'id');
    }
}