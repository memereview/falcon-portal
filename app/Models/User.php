<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    const TYPE_ADMIN    = 'ADMIN';
    const TYPE_CLIENT   = 'CLIENT';
    const TYPE_RIDER    = 'RIDER';

    const VALID_WEB_USERS = [
        User::TYPE_ADMIN,
        User::TYPE_CLIENT
    ];

    const VALID_API_USERS = [
        User::TYPE_RIDER
    ];


    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'middle_name', 'last_name', 'email', 'type', 'picture'
    ];

    protected $dates = ['deleted_at'];

    /**
     * Get the fields and values needed for the session data of the user
     *
     * @return array
     */
    public function fieldsForSession()
    {
        return [
            'id'            => $this->id,
            'first_name'    => $this->first_name,
            'middle_name'   => $this->middle_name,
            'last_name'     => $this->last_name,
            'email'         => $this->email,
            'type'          => $this->type,
            'picture'       => $this->picture
        ];
    }

}
