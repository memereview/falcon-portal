<?php
/**
 * Created by PhpStorm.
 * User: Sherwin
 * Date: 3/3/2018
 * Time: 4:05 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CustomerAddress extends Model
{
    use Notifiable;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'phone', 'email','street', 'street2', 'barangay',
        'city', 'province', 'postal', 'country'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}