<?php

namespace App\Providers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        //Add this custom validation rule for alphanumeric with spaces.
        Validator::extend('alpha_num_space', function ($attribute, $value) {
            // This will only accept alphanumeric and spaces.
            return preg_match('/^[a-zA-Z0-9\s]+$/u', $value);
        });

        //Add this custom validation rule for the hashing of password
        Validator::extend('hash_check', function ($attribute, $value, $parameters) {
            return Hash::check($value, $parameters[0]);
        });

        //Add this custom validation rule for checking if the current password matches
        //the input password
        Validator::extend('current_password_check', function ($attribute, $value, $parameters) {
            // check if the password matches
            if (strcmp($value, $parameters[0]) == 0) {
                return false;
            }

            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
