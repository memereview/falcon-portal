(function ($) {
    'use strict';

    var delay = 100,
        loginForm = $('#login-form'),
        forgotPasswordForm = $('#forgot-password-form');

    /**
     * Forgot password link click
     */
    $('#forgot-password').click(onForgotPasswordClick);

    /**
     * Back to login link click
     */
    $('#back-to-login').click(onBackToLoginClick);

    /**
     * Forgot password form submit
     */
    forgotPasswordForm.submit(onForgotPasswordFormSubmit);

    /**
     * Functions called by the elements
     */

    /**
     * Forgot password click
     *
     * @param e
     */
    function onForgotPasswordClick(e) {
        // prevent from doing anything
        e.preventDefault();

        // add class
        $('body').addClass('show-forgot-password');

        // hide login form. animate the hiding process
        loginForm.animate({
            left: '-50px',
            opacity: 0
        }, delay, function () {
            $(this).hide();

            // clean form
            cleanForm();

            // show the forgot password form
            forgotPasswordForm.delay(delay)
                .show()
                .animate({
                    left: '0',
                    opacity: 1
                }, delay);
        });
    }

    /**
     * Back to login click
     *
     * @param e
     */
    function onBackToLoginClick(e) {
        // prevent from doing anything
        e.preventDefault();

        // remove class
        $('body').removeClass('show-forgot-password');

        // hide forgot password form. animate the hiding process
        forgotPasswordForm.animate({
            left: '50px',
            opacity: 0
        }, delay, function () {
            $(this).hide();

            // show the login form
            loginForm.delay(delay)
                .show()
                .animate({
                    left: '0',
                    opacity: 1
                }, delay);

            // clean the forgot password form
            cleanForm();
        });
    }

    /**
     * Submitting the forgot password form
     *
     * @param e
     */
    function onForgotPasswordFormSubmit(e) {
        // prevent from doing anything
        e.preventDefault();

        // clean form UI but not the values
        removeAlert();
        removeFeedback();
        removeIsInvalid();

        $.post(
            '/forgot-password',
            forgotPasswordForm.serialize()
        ).done(function (result) {
            // replace the current forgot password form container
            // with the result from the ajax
            forgotPasswordForm.replaceWith(result);

            // reassign the variable using the selector and
            // show the form
            forgotPasswordForm = $('#forgot-password-form')
                .show()
                .css({
                    left: 0,
                    opacity: 1
                });

            // reset the events
            // back to login
            $('#back-to-login').click(onBackToLoginClick);
            // for submission
            forgotPasswordForm.submit(onForgotPasswordFormSubmit);
        });
    }

    function cleanForm() {
        removeAlert();
        removeFeedback();
        removeIsInvalid();
        cleanFormValues();
    }

    function removeAlert() {
        $('.alert').remove();
    }

    function removeFeedback() {
        $('.invalid-feedback').html('');
    }

    function removeIsInvalid() {
        $('.is-invalid').removeClass('is-invalid');
    }

    function cleanFormValues() {
        $('input').not('[type=hidden]').val('');
    }

})(jQuery);