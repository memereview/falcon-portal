(function ($) {

    $(document).ready(function () { 
        $('.unit-verify').submit(function (e) {
            e.preventDefault();

            var $form = $(this);
            var manifestNumber = $form.find('[name=manifest_number]').val();
            var trackingNumber = $form.find('[name=tracking_number]').val();
            var verificationUrl = '/manifests/' + manifestNumber + '/units/' + trackingNumber + '/verify';
            
            $.ajax({
                type: 'post',
                url: verificationUrl,
            }).done(function(response) {
                if (!response.error) {
                    swal(
                        'Verified!',
                        response.response['unitVerified'],
                        'success'
                    );

                    // set the status to verified
                    $('#unit-' + trackingNumber).html('VERIFIED');

                    // add the hidden attribute
                    $form.attr('hidden', 'hidden');

                    $form.parent().parent().addClass('table-success');
                }

                var manifestStatus = response.response['checkManifest'];

                if (manifestStatus == true) {
                    
                    var verificationUrl = '/manifests/' + manifestNumber + '/verify';

                    $.ajax({
                        type: 'post',
                        url: verificationUrl,
                    }).done(function(response){
                        swal(response.response);

                        var url = '/manifests/current/units/' + manifestNumber;
                        $(location).attr('href',url);
                    });
                    
                }
            });
        })
    });

})(jQuery);