(function ($) {

    $(document).ready(function () { 
        $('.delivery-verify').submit(function (e) {
            e.preventDefault();

            var $form = $(this);
            var trackingNumber = $form.find('[name=tracking_number]').val();
            var verificationUrl = '/units/' + trackingNumber + '/delivery-verify';
            
            $.ajax({
                type: 'post',
                url: verificationUrl,
            }).done(function(response) {
                if (!response.error) {
                    swal(
                        'Confirmed Delivery!',
                        response.response['deliveryVerified'],
                        'success'
                    );

                    // set the status to verified
                    $('#unit-' + trackingNumber).html('DELIVERED');

                    // add the hidden attribute
                    $form.attr('hidden', 'hidden');

                    $form.parent().parent().addClass('table-success');
                }

                // var manifestStatus = response.response['checkManifest'];

                // if (manifestStatus == true) {
                    
                //     var verificationUrl = '/manifests/' + manifestNumber + '/verify';

                //     $.ajax({
                //         type: 'post',
                //         url: verificationUrl,
                //     }).done(function(response){
                //         swal(response.response);

                //         var url = '/manifests/current/units/' + manifestNumber;
                //         $(location).attr('href',url);
                //     });
                    
                // }
            });
        })
    });

})(jQuery);