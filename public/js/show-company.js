(function ($) {
    'use strict';

    $(document).ready(function () {
        var company = $('#company');

    	$('select#type').on('change', function () {
            if ($(this).val() === 'CLIENT') {
                company.show();
            } else {
                company.hide();
            }
        });

    });
})(jQuery);