function showNewUnitDetails(number){
    document.location.href = "/manifests/new/units/" + number;
}
function showCurrentUnitDetails(number){
    document.location.href = "/manifests/current/units/" + number;
}
function showArchivedUnitDetails(number){
    document.location.href = "/manifests/archived/units/" + number;
}
function showUsersDetails(id){
	document.location.href = "/admin/users/" + id;
}
function showUserDetails(id){
	document.location.href = "/admin/users/" + id;
}