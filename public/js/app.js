(function ($) {
    'use strict';

    /**
     * Global setup for ajax
     */
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $(document).ready(function () {
        // set the initial time
        setHeaderDateTime();

        // update the date and time every second
        setInterval(function () {
            setHeaderDateTime();
        }, 1000);

        // on sidebar toggle click
        $('#sidebar-toggle').click(function (e) {
            // prevent default action
            e.preventDefault();

            // toggle 'toggled' class in body
            $('body').toggleClass('toggled');
        });

        // enable tooltips
        $('[data-toggle="tooltip"]').tooltip();
    });

    /**
     * Update the header's date and time
     */
    function setHeaderDateTime() {
        var date = new Date(),
            dateSpan = $('header #date'),
            timeSpan = $('header #time');

        dateSpan.html($.format.date(date, 'ddd, dd MMMM yyyy'));
        timeSpan.html($.format.date(date, 'hh:mm:ss a'));
    }

})(jQuery);