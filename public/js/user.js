(function ($) {
  'use strict';

  $(document).ready(function() {

    // on data-delete-id click, show swal first
    $('[data-delete-id]').click(function () {
      var userId = $(this).data('delete-id');

      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this action!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirm'
      }).then(function (result) {
        if (result.value) {
          $.ajax({
            url: '/admin/users/' + userId,
            type: 'DELETE'
          }).then(function (result) {
            if (result.code === 200) {
              location.reload();
            }
          })
        }
      })
    });

  });

})(jQuery);