(function ($) {
    'use strict';

    $(document).ready(function () {
        // set the units as sortable
        $('#units').sortable({
            animation: 150,
            draggable: '.dragged',
            group: 'assign'
        });

        // set the riders as sortable
        $('.riders').sortable({
            animation: 150,
            draggable: '.dragged',
            group: 'assign'
        });

        $("#auto-assign").click(function () {
            $.post(
                '/admin/auto-assign',
            ).done(function (data) {
                swal(data.response);
                console.log(data.response);
                location.reload();
                
            });
        });

        $("#clear").click(function () {
            $.get(
                '/admin/clear-assign',
            ).done(function (data) {
                swal(data.response);
                console.log(data.response);
                location.reload();
            });
        });

        // on submit of the form
        $('#assign-units').submit(function (e) {
            // prevent default form action
            e.preventDefault();

            var riderContainer = $('.assigned-container'),
                set = [];

            riderContainer.each(function () {
                //getting rider's id
                var rider = $(this).find('.riders').first();
                var riderId = rider.data('rider');

                //check for units
                var units = rider.find('[data-unit]');

                // each units, get the unit id and push to the
                // set to be sent to the server
                units.each(function () {
                    var unitId = $(this).data('unit');

                    set.push([riderId, unitId]);
                });
            });

            $.post(
                '/admin/assign-units',
                { 'set': set }
            ).done(function (data) {
                swal(data.response);
                console.log(data.response);
            });
        })
    });
})(jQuery);