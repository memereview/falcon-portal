(function ($) {
    'use strict';

    $(document).ready(function () {
        $('#profile-picture').on('click', function () {
            $('#picture-input')[0].click();
        });

        // on profile picture input change
        $('#picture-input').on('change', function () {
            var file = this.files[0];
            var formData = new FormData();
            formData.append('picture', file);

            $.post({
                url: '/upload/profile-picture',
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            }).done(function (result) {
                if (result.error) {
                    swal(
                        'Error',
                        result.response,
                        'error'
                    )
                } else {
                    $('#profile-picture, #user-picture').css('background-image', "url('/images/" + result.response + "'");
                }
            })
        });
    })
})(jQuery);